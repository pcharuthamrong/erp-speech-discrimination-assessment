# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 15:09:18 2021

@author: DELL
"""

import random

possible_sequences = [letter + str(i+1)for letter in ('a', 'b', 'c') for i in range(10)]
available_sequences = possible_sequences.copy()

random.seed()

def getRandomSequence():
    if not available_sequences:
        print('no available sequences')
        return
    
    choice_sequence = random.choice(available_sequences)
    available_sequences.remove(choice_sequence)
    print(choice_sequence)
    print('Remaining: ' + str.join(', ', available_sequences))