clc;
filenameFile = '..\\data\\summary\\filename.xlsx';
excludeFile = '..\\data\\summary\\manual_inspection.xlsx';
path = '..\\data\\psychopy';

% turn warning for modified varnames off
warning('off','MATLAB:table:ModifiedAndSavedVarnames');
% read filenames from excel sheet
opts = detectImportOptions(filenameFile);
opts = setvartype(opts, {'experiment'}, 'char');
filenameTbl = readtable(filenameFile, opts);
opts = detectImportOptions(excludeFile);
opts = setvartype(opts, {'experiment'}, 'char');
opts.DataRange = 'A2:N90';
excludeTbl = readtable(excludeFile, opts);
% turn warning for modified varnames back on
warning('on','MATLAB:table:ModifiedAndSavedVarnames');

% select desired columns
filenameTbl = filenameTbl(:,1:6);
excludeTbl = [excludeTbl(:,1:4) excludeTbl(:,end-1)];
% join table
dataTbl = innerjoin(filenameTbl, excludeTbl, 'Keys', {'participant', 'group', 'session', 'experiment'});
% exclude entries where finalDecision = 'no'
dataTbl = dataTbl(strcmp(dataTbl.finalDecision, 'yes'), :);

% compute accuracy
accTbl = rowfun(@computeAccuracy, dataTbl, 'InputVariables', {'corrAns', 'experiment'}, 'OutputVariableNames', 'accuracy');
dataTbl = [dataTbl accTbl];
% compute stat
statTbl = grpstats(dataTbl, 'experiment', {'mean', 'std'}, 'DataVars', {'corrAns', 'rt'});
disp(statTbl);

accArray = dataTbl.accuracy;
rtArray = dataTbl.rt;
group = dataTbl.experiment;
% map group name to number
groupMap = containers.Map({'1a', '1b', '1c', '2', '3'}, {1, 2, 3, 4, 5});
groupNum = zeros(height(group),1);
for i = 1:height(group)
    groupNum(i) = groupMap(group{i});
end

compareAndDisplay(rtArray, groupNum, 'Response time');
compareAndDisplay(accArray, groupNum, 'Behavioral accuracy');

function acc = computeAccuracy(corrAns, method)
    fullScoreMap = containers.Map({'1a', '1b', '1c', '2', '3'}, {80, 48, 80, 80, 150});
    acc = corrAns / fullScoreMap(method{:});
end
function compareAndDisplay(data, group, compVarName)
    alpha = 0.05;
    cType = 'bonferroni';
    fprintf('Multiple comparison of means - %s\nCType: %s\n', compVarName, cType);
    fprintf('Numbered group: 1a, 1b, 1c, 2, 3\n');
    [p,anovaTbl,stats] = anova1(data, group, 'off');
    [c,m,h] = multcompare(stats, 'Alpha', alpha, 'CType', cType, 'Display', 'off');
    result = array2table(c, 'VariableNames', {'A', 'B' , 'Lower CI', 'estimate', 'Upper CI', 'p-value'});
    disp(result);
    disp(anovaTbl);
    disp(stats);
end