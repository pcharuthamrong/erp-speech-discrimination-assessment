filename = '..\\data\\summary\\preprocessed_filename.xlsx';
opts = detectImportOptions(filename);
opts.DataRange = 'A2';
opts = setvartype(opts, {'folder', 'filename'}, 'char');
table = readtable(filename, opts);

for i = 1:height(table)
    current_filename = table.filename{i};
    current_filename_notype = table.filename{i}(1:end-4);
    current_folder = table.folder{i};
    
    eeglab_export(current_filename, current_folder, current_filename_notype);
end

function eeglab_export(filename, folder, setname)
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    EEG = pop_loadset('filename', filename ,'filepath',strcat('..\\data\\eeglab\\interpolated\\', folder, '\\'));
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );
    pop_export(EEG,strcat('..\\data\\preprocessed\\', folder, '\\', setname, '.csv'),'transpose','on','separator',',','precision',4);
    EEG = eeg_checkset( EEG );
    pop_expevents(EEG, strcat('..\\data\\preprocessed\\', folder, '\\', setname, '.txt'), 'samples');
    eeglab redraw;
end
