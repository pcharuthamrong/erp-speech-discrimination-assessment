function eventId = eventIdentifiers()
    % event identifiers
    eventId.SOUND1_LOUD = 1;
    eventId.SOUND2_LOUD = 2;
    eventId.SOUND1_QUIET = 3;
    eventId.SOUND2_QUIET = 4;

    eventId.IMAGE_CHICKEN = 17;
    eventId.IMAGE_EGG = 18;
    eventId.IMAGE_HORSE = 19;
    eventId.IMAGE_DOG = 20;
    eventId.IMAGE_WORD1 = 21;
    eventId.IMAGE_WORD2 = 22;

    eventId.MATCH = 26;
    eventId.MISMATCH = 27;
end