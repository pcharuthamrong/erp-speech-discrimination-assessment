filename = '..\\data\\summary\\filename.csv';
opts = detectImportOptions(filename);
opts = setvartype(opts, {'participant', 'experiment'}, 'char');
filename_table = readtable(filename, opts);

for row = 1:height(filename_table)
    participant = filename_table.participant{row};
    group = filename_table.group{row};
    session = filename_table.session(row);
    experiment = filename_table.experiment{row};
    csv = filename_table.csvFile{row};
    setname = sprintf('p%ss%de%s', participant, session, experiment);
    
    import_and_preprocess(csv, setname, participant, session, experiment, group);
end
