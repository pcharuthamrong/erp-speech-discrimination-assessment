filename = '..\\data\\summary\\preprocessed_filename.xlsx';
opts = detectImportOptions(filename);
opts.DataRange = 'A2';
opts = setvartype(opts, {'folder', 'filename'}, 'char');
table = readtable(filename, opts);

for i = 1:height(table)
    current_filename = table.filename{i};
    current_folder = table.folder{i};
    
    interpolate_channels(current_filename, current_folder);
end

function interpolate_channels(filename, folder)
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    EEG = pop_loadset('filename','p1s1e1a.set','filepath','..\\data\\eeglab\\raw\\');
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = pop_loadset('filename',filename,'filepath',strcat('..\\data\\eeglab\\manual_inspect\\', folder, '\\'));
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );
    EEG = pop_interp(EEG, ALLEEG(1).chanlocs, 'spherical');
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'savenew',strcat('..\\data\\eeglab\\interpolated\\', folder, '\\', filename),'gui','off'); 
    eeglab redraw;
end