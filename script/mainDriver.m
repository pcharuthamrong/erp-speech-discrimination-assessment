clc;
filenameFile = '..\\data\\summary\\preprocessed_filename.xlsx';
path = '..\\data\\preprocessed\\';

% options
method = '1a';
validationType = 'KFold'; %options: 'KFold' 'Holdout'
foldCount = 5;
holdoutP = 0.2;
featureSet = 'time+freq40'; %options: 'raw' 'time' 'time+freq30' 'time+freq40'
classifierType = 'LDA'; %options: 'SVM' 'LDA' 'DLDA' 'Shrinkage LDA'
costMatrix = [0 1; 1 0];
applyCSP = false;
passband = [1 40];

chArray = 2:9;
samplingRate = 256;
framePerEpochMap = containers.Map({'1a', '1b', '1c', '2', '3'}, {256, 256, 256, 256, 128});
framePerEpoch = framePerEpochMap(method);
participantCountMap = containers.Map({'1a', '1b', '1c', '2', '3'}, {9, 10, 7, 25, 26});
participantCount = participantCountMap(method);
channelCount = 8;

filenameTable = readFilename(filenameFile, method);

perfScores = zeros(participantCount,2); % row:participants; col: participant, acc
confMatrices = cell(participantCount,1);
for i = 1:2:height(filenameTable)
    for j = 0:1
        filename = filenameTable.filename{i+j}(1:end-4);
        folder = filenameTable.folder{i+j};
        setname = filename(1:strfind(filename, '_') - 1);
        participant = filename(2:strfind(filename, 's') - 1);
        % 1:match/standard, 2:mismatch/deviant
        conditionName = filename(strfind(filename, '_') + 1:end);
        if (strcmp(conditionName, 'match') || strcmp(conditionName, 'standard'))
            [eegTable1, epochCount1] = readAndNormalize(path, folder, filename, framePerEpoch, chArray);
        elseif (strcmp(conditionName, 'mismatch') || strcmp(conditionName, 'deviant'))
            [eegTable2, epochCount2] = readAndNormalize(path, folder, filename, framePerEpoch, chArray);
        end
    end
    % update frame per epoch to match the cut epochs
    framePerEpochCut = height(eegTable1(eegTable1.Epoch == 1,:)); % equal frames over all epoch, condition
    % apply additional filter if desired
    if(~isequal(passband, [1 40]))
        eegTable1(:,chArray) = applyAdditionalFilter(eegTable1, passband, samplingRate, chArray);
        eegTable2(:,chArray) = applyAdditionalFilter(eegTable2, passband, samplingRate, chArray);
    end
    
    % partition
    rng('default');
    if(strcmp(validationType, 'Holdout'))
        cvp1 = cvpartition(epochCount1, 'Holdout', holdoutP);
        cvp2 = cvpartition(epochCount2, 'Holdout', holdoutP);
        foldCount = 1;
    elseif(strcmp(validationType, 'KFold'))
        cvp1 = cvpartition(epochCount1, 'KFold', foldCount);
        cvp2 = cvpartition(epochCount2, 'KFold', foldCount);
    end
    
    % for each fold:    transform using CSP, extract features, oversampling
    %                   , train model, predict & evaluate model
    confMatrix = [0 0; 0 0];
    avgAcc = 0;
    for fold = 1:foldCount
        [trainingSet1, testSet1] = getTrainTest(eegTable1, cvp1, fold, framePerEpochCut);
        [trainingSet2, testSet2] = getTrainTest(eegTable2, cvp2, fold, framePerEpochCut);
        if(applyCSP)
            % train CSP using only training sets
            [W, lambda, A] = trainCSP(trainingSet1, trainingSet2, chArray);
            % apply learned mixing matrix W to training & test sets
            trainingCSP1 = transformCSP(trainingSet1, W, chArray);
            trainingCSP2 = transformCSP(trainingSet2, W, chArray);
            testCSP1 = transformCSP(testSet1, W, chArray);
            testCSP2 = transformCSP(testSet2, W, chArray);
        else
            trainingCSP1 = trainingSet1;
            trainingCSP2 = trainingSet2;
            testCSP1 = testSet1;
            testCSP2 = testSet2;
        end
        % extract features
        trainingData1 = extractFeature(trainingCSP1, featureSet, method, channelCount, 1, samplingRate, chArray);
        trainingData2 = extractFeature(trainingCSP2, featureSet, method, channelCount, 2, samplingRate, chArray);
        testData1 = extractFeature(testCSP1, featureSet, method, channelCount, 1, samplingRate, chArray);
        testData2 = extractFeature(testCSP2, featureSet, method, channelCount, 2, samplingRate, chArray);
        % combine the 2 conditions
        trainingData = [trainingData1; trainingData2];
        testData = [testData1; testData2];
        % shuffle order
        rng('default');
        trainingData = trainingData(randperm(size(trainingData,1)), :);
        testData = testData(randperm(size(testData,1)), :);
        % oversampling
        oversampledTrainingData = oversampling(trainingData);
        % train classifier
        trainedClassifier = trainClassifier(oversampledTrainingData, classifierType, costMatrix);
        % predict & evaluate
        predictedClass = predict(trainedClassifier,testData(:, 1:end-1));
        [foldAccuracy, foldConfMatrix] = evaluateClassification(predictedClass, testData{:, end});
        avgAcc = avgAcc + foldAccuracy;
        confMatrix = confMatrix + foldConfMatrix;
    end
    avgAcc = avgAcc / foldCount;
    perfScores((i-1)/2+1,1) = str2double(participant);
    perfScores((i-1)/2+1,2) = avgAcc;
    confMatrices{(i-1)/2+1} = confMatrix;
end
displayResult(perfScores, method, foldCount, featureSet, classifierType, applyCSP, passband, validationType, holdoutP);

function filenameTable = readFilename(filenameFile, folderSelect)
    % read filenames from excel sheet
    opts = detectImportOptions(filenameFile);
    opts.DataRange = 'A2';
    opts = setvartype(opts, {'folder', 'filename'}, 'char');
    wholeFilenameTable = readtable(filenameFile, opts);
    filenameTable = wholeFilenameTable(strcmp(wholeFilenameTable.folder, folderSelect), :);
end
function dataTbl = readEegFile(path, folder, filename, framePerEpoch)
    % read file
    file = strcat(path, folder, '\\', filename, '.csv');
    opts = detectImportOptions(file);
    dataTbl = readtable(file, opts);
    % add epoch label to each row
    epochCount = height(dataTbl) / framePerEpoch;
    temp = [];
    for i = 1:epochCount
        temp = [temp;repmat(i, framePerEpoch, 1)];
    end
    dataTbl.Epoch = temp;
end
function [eegTbl, epochCount] = readAndNormalize(path, folder, filename, framePerEpoch, chArray)
    dataTbl = readEegFile(path, folder, filename, framePerEpoch);
    epochCount = height(dataTbl) / framePerEpoch;
    
    % remove frame before stimulus onset & normalize data (z-score; mean = 0, sd = 1)
    eegTblUnnorm = dataTbl(dataTbl.Time > 0, :);
    eegTbl = normalize(eegTblUnnorm, 'DataVariables', chArray);
end
function newTbl = applyAdditionalFilter(dataTbl, passband, samplingRate, chArray)
    newTbl = varfun(@(x) bandpass(x, passband, samplingRate), dataTbl, 'InputVariables', chArray);
end
function [trainingSet, testSet] = getTrainTest(dataset, cvp, fold, framePerEpoch)
    trainingIdx = repelem(training(cvp, fold), framePerEpoch);
    testIdx = repelem(test(cvp, fold), framePerEpoch);
    
    trainingSet = dataset(trainingIdx, :);
    testSet = dataset(testIdx, :);
end
function [W, lambda, A] = trainCSP(dataTbl1, dataTbl2, chArray)
    dataArray1 = table2array(dataTbl1(:, chArray))';
    dataArray2 = table2array(dataTbl2(:, chArray))';
    
    [W, lambda, A] = csp(dataArray1, dataArray2);
end
function dataCSP = transformCSP(dataTbl, W, chArray)
    dataArray = table2array(dataTbl(:, chArray))';
    cspArray = W'*dataArray;
    dataCSP = [dataTbl(:,1), array2table(cspArray'), dataTbl(:,end)];
    dataCSP.Properties.VariableNames = dataTbl.Properties.VariableNames;
end
function newTbl = oversampling(trainingData)
    % identify minority class & target count
    class1Count = sum(trainingData.Condition == 1);
    class2Count = sum(trainingData.Condition == 2);
    if (class1Count > class2Count)
        targetCount = class1Count;
        minClass = 2;
    else
        targetCount = class2Count;
        minClass = 1;
    end
    
    % oversampling minority class
    rng('default');
    oversampledMinClass = datasample(trainingData(trainingData.Condition == minClass, :), targetCount);
    % reassemble oversampledMinClass into original table
    osTbl = [trainingData(trainingData.Condition ~= minClass, :); oversampledMinClass];
    rng('default');
    newTbl = osTbl(randperm(size(osTbl,1)), :);
end
function [acc, confMatrix] = evaluateClassification(predictedClass, actualClass)
    % Compute validation accuracy
    correctPredictions = (predictedClass == actualClass);
    isMissing = isnan(actualClass);
    correctPredictions = correctPredictions(~isMissing);
    acc = sum(correctPredictions)/length(correctPredictions);
    
    % Compute confusion matrix
    confMatrix = confusionmat(actualClass, predictedClass);
end
function displayResult(perfMatrix, method, foldCount, featureSet, classifierType, applyCSP, passband, validationType, holdoutP)
    fprintf('Method: %s\t\t\t\tClassifier: %s\nValidation: %s\t\t', method, classifierType, validationType);
    if(strcmp(validationType, 'Holdout'))
        fprintf('Holdout: %.2f\n', holdoutP);
    elseif(strcmp(validationType, 'KFold'))
        fprintf('Fold: %d\n', foldCount);
    end
    fprintf('Features: %s\t\t\tCSP: %s\nPassband: %d-%d Hz\n\n', featureSet, string(applyCSP), passband);
    % format & display accuracy
    result = table(perfMatrix(:,1), perfMatrix(:,2), 'VariableNames', {'Participant', 'Validation Accuracy'});
    disp(result);
end
function trainedClassifier = trainClassifier(trainingData, classifierType, costMatrix)
    predictors = trainingData(:,1:end-1);
    response = trainingData.Condition;
    
    if(strcmp(classifierType, 'SVM'))
        trainedClassifier = fitcsvm(...
            predictors, ...
            response, ...
            'KernelFunction', 'linear', ...
            'PolynomialOrder', [], ...
            'KernelScale', 'auto', ...
            'BoxConstraint', 1, ...
            'Standardize', true, ...
            'Cost', costMatrix, ...
            'ClassNames', [1; 2]);
    elseif(strcmp(classifierType, 'LDA'))
        trainedClassifier = fitcdiscr(...
            predictors, ...
            response, ...
            'DiscrimType', 'linear', ...
            'Gamma', 0, ...
            'FillCoeffs', 'off', ...
            'Cost', costMatrix, ...
            'ClassNames', [1; 2]);
    elseif(strcmp(classifierType, 'DLDA'))
        trainedClassifier = fitcdiscr(...
            predictors, ...
            response, ...
            'DiscrimType', 'diagLinear', ...
            'Gamma', 0, ...
            'FillCoeffs', 'off', ...
            'Cost', costMatrix, ...
            'ClassNames', [1; 2]);
    elseif(strcmp(classifierType, 'Shrinkage LDA'))
        trainedClassifier = fitcdiscr(...
            predictors, ...
            response, ...
            'DiscrimType', 'linear', ...
            'Gamma', 0, ...
            'FillCoeffs', 'off', ...
            'Cost', costMatrix, ...
            'ClassNames', [1; 2]);
        [err,gamma,delta,numpred] = cvshrink(trainedClassifier,...
            'NumGamma',9,'NumDelta',9,'Verbose',0);
    end
end