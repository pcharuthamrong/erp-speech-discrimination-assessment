function predictor_table = extract_time_domain_feature(eeg_table, tp1, tp2, ch_array)
    suffix1 = '_tp1';
    suffix2 = '_tp2';
    
    % mean amplitude
    mean_amp_tp1 = varfun(@mean, eeg_table(tp1, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    mean_amp_tp1 = concat_col_name('', mean_amp_tp1, suffix1);
    mean_amp_tp2 = varfun(@mean, eeg_table(tp2, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    mean_amp_tp2 = concat_col_name('', mean_amp_tp2, suffix2);
    
    % variance
    variance_tp1 = varfun(@var, eeg_table(tp1, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    variance_tp1 = concat_col_name('', variance_tp1, suffix1);
    variance_tp2 = varfun(@var, eeg_table(tp2, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    variance_tp2 = concat_col_name('', variance_tp2, suffix2);
    
    % max absolute amplitude
    abs_eeg_table = [eeg_table(:, 1), varfun(@abs, eeg_table, 'InputVariables', ch_array), eeg_table(:, 10)];
    max_abs_amp_tp1 = varfun(@max, abs_eeg_table(tp1, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    max_abs_amp_tp1 = concat_col_name('', max_abs_amp_tp1, suffix1);
    max_abs_amp_tp2 = varfun(@max, abs_eeg_table(tp2, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    max_abs_amp_tp2 = concat_col_name('', max_abs_amp_tp2, suffix2);
    
    % peak latency
    peak_lat_tp1 = varfun(@(x) get_peak_latency(x, abs_eeg_table(tp1, :)), abs_eeg_table(tp1, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    peak_lat_tp1 = concat_col_name('peak_lat_', peak_lat_tp1, suffix1);
    peak_lat_tp2 = varfun(@(x) get_peak_latency(x, abs_eeg_table(tp2, :)), abs_eeg_table(tp2, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    peak_lat_tp2 = concat_col_name('peak_lat_', peak_lat_tp2, suffix2);
    
    % mp ratio = max absolute amplitude / peak latency
    mp_tp1 = varfun(@(x) get_mp_ratio(x, abs_eeg_table(tp1, :)), abs_eeg_table(tp1, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    mp_tp1 = concat_col_name('mp_', mp_tp1, suffix1);
    mp_tp2 = varfun(@(x) get_mp_ratio(x, abs_eeg_table(tp2, :)), abs_eeg_table(tp2, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    mp_tp2 = concat_col_name('mp_', mp_tp2, suffix2);
    
    % positive area
    pos_eeg_table = [eeg_table(:, 1), varfun(@(x) max(x, 0), eeg_table, 'InputVariables', ch_array), eeg_table(:, 10)];
    pos_area_tp1 = varfun(@trapz, pos_eeg_table(tp1, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    pos_area_tp1 = concat_col_name('pos_', pos_area_tp1, suffix1);
    pos_area_tp2 = varfun(@trapz, pos_eeg_table(tp2, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    pos_area_tp2 = concat_col_name('pos_', pos_area_tp2, suffix2);
    
    % negative area
    neg_eeg_table = [eeg_table(:, 1), varfun(@(x) min(x, 0), eeg_table, 'InputVariables', ch_array), eeg_table(:, 10)];
    neg_area_tp1 = varfun(@trapz, neg_eeg_table(tp1, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    neg_area_tp1 = concat_col_name('neg_', neg_area_tp1, suffix1);
    neg_area_tp2 = varfun(@trapz, neg_eeg_table(tp2, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
    neg_area_tp2 = concat_col_name('neg_', neg_area_tp2, suffix2);
    
    predictor_table = [mean_amp_tp1(:, 3:end), mean_amp_tp2(:, 3:end), ...
        variance_tp1(:, 3:end), variance_tp2(:, 3:end), ...
        max_abs_amp_tp1(:, 3:end), max_abs_amp_tp2(:, 3:end), ...
        peak_lat_tp1(:, 3:end), peak_lat_tp2(:, 3:end), ...
        mp_tp1(:, 3:end), mp_tp2(:, 3:end), ...
        pos_area_tp1(:, 3:end), pos_area_tp2(:, 3:end), ...
        neg_area_tp1(:, 3:end), neg_area_tp2(:, 3:end)];
end

function lat = get_peak_latency(tbl, source_tbl)
    [~, i] = max(tbl);
    lat = source_tbl.Time(i);
end
function mp = get_mp_ratio(tbl, source_tbl)
    [amp, i] = max(tbl);
    lat = source_tbl.Time(i);
    mp = amp / lat;
end