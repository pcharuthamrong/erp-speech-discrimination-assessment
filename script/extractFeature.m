function featureTbl = extractFeature(dataTbl, featureSet, method, channelCount, condition, samplingRate, chArray)
    if (strcmp(method, '3'))
        tp1s = 100; tp1e = 250;
        tp2s = 250; tp2e = 400;
    else
        tp1s = 200; tp1e = 400;
        tp2s = 500; tp2e = 800;
    end
    tp1 = dataTbl.Time >= tp1s & dataTbl.Time <= tp1e;
    tp2 = dataTbl.Time >= tp2s & dataTbl.Time <= tp2e;
    
    featureTbl = table();
    
    if(strcmp(featureSet, 'raw'))
        % raw features
        epochCount = height(unique(dataTbl.Epoch));
        featureTbl = extractRawFeature(dataTbl, epochCount, channelCount);
    elseif(strcmp(featureSet, 'time'))
        % time domain features
        featureTbl = extractTimeDomainFeature(dataTbl, tp1, tp2, chArray);
    elseif(strcmp(featureSet, 'time+freq30'))
        featureTbl = [extractTimeDomainFeature(dataTbl, tp1, tp2, chArray), ...
            extractFreqDomainFeature(dataTbl, tp1, tp2, chArray, samplingRate, 30)];
    elseif(strcmp(featureSet, 'time+freq40'))
        featureTbl = [extractTimeDomainFeature(dataTbl, tp1, tp2, chArray), ...
            extractFreqDomainFeature(dataTbl, tp1, tp2, chArray, samplingRate, 40)];
    end

    % add condition label to each row
    featureTbl.Condition = repmat(condition, height(featureTbl), 1);
end
function featureTbl = extractRawFeature(dataTbl, epochCount, channelCount)
    epochArray = unique(dataTbl.Epoch);
    colPerCh = size(dataTbl{dataTbl.Epoch == epochArray(1),2}, 1);
    colCount = colPerCh * channelCount;
    featureTbl = table('Size', [epochCount colCount], 'VariableTypes', repmat(["double"], 1, colCount));
    for e = 1:epochCount
        for c = 1:channelCount
            startCol = colPerCh * (c - 1) + 1;
            endCol = colPerCh * c;
            featureTbl(e, startCol:endCol) = num2cell(dataTbl{dataTbl.Epoch == epochArray(e),c+1}');
        end
    end
end
function featureTbl = extractTimeDomainFeature(dataTbl, tp1, tp2, chArray)
    suffix1 = '_tp1';
    suffix2 = '_tp2';
    
    % mean amplitude
    meanAmpTp1 = varfun(@mean, dataTbl(tp1, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    meanAmpTp1 = concatColName('', meanAmpTp1, suffix1);
    meanAmpTp2 = varfun(@mean, dataTbl(tp2, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    meanAmpTp2 = concatColName('', meanAmpTp2, suffix2);
    % variance
    varianceTp1 = varfun(@var, dataTbl(tp1, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    varianceTp1 = concatColName('', varianceTp1, suffix1);
    varianceTp2 = varfun(@var, dataTbl(tp2, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    varianceTp2 = concatColName('', varianceTp2, suffix2);
    
    % max absolute amplitude
    absTable = [dataTbl(:, 1), varfun(@abs, dataTbl, 'InputVariables', chArray), dataTbl(:, 10)];
    maxAbsAmpTp1 = varfun(@max, absTable(tp1, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    maxAbsAmpTp1 = concatColName('', maxAbsAmpTp1, suffix1);
    maxAbsAmpTp2 = varfun(@max, absTable(tp2, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    maxAbsAmpTp2 = concatColName('', maxAbsAmpTp2, suffix2);
    
    % peak latency
    peakLatTp1 = varfun(@(x) getPeakLatency(x, absTable(tp1, :)), absTable(tp1, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    peakLatTp1 = concatColName('peakLat_', peakLatTp1, suffix1);
    peakLatTp2 = varfun(@(x) getPeakLatency(x, absTable(tp2, :)), absTable(tp2, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    peakLatTp2 = concatColName('peakLat_', peakLatTp2, suffix2);
    
    % mp ratio = max absolute amplitude / peak latency
    mpTp1 = varfun(@(x) getMpRatio(x, absTable(tp1, :)), absTable(tp1, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    mpTp1 = concatColName('mp_', mpTp1, suffix1);
    mpTp2 = varfun(@(x) getMpRatio(x, absTable(tp2, :)), absTable(tp2, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    mpTp2 = concatColName('mp_', mpTp2, suffix2);
    
    % positive area
    posTable = [dataTbl(:, 1), varfun(@(x) max(x, 0), dataTbl, 'InputVariables', chArray), dataTbl(:, 10)];
    posAreaTp1 = varfun(@trapz, posTable(tp1, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    posAreaTp1 = concatColName('pos_', posAreaTp1, suffix1);
    posAreaTp2 = varfun(@trapz, posTable(tp2, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    posAreaTp2 = concatColName('pos_', posAreaTp2, suffix2);
    
    % negative area
    negTable = [dataTbl(:, 1), varfun(@(x) min(x, 0), dataTbl, 'InputVariables', chArray), dataTbl(:, 10)];
    negAreaTp1 = varfun(@trapz, negTable(tp1, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    negAreaTp1 = concatColName('neg_', negAreaTp1, suffix1);
    negAreaTp2 = varfun(@trapz, negTable(tp2, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    negAreaTp2 = concatColName('neg_', negAreaTp2, suffix2);
    
    featureTbl = [meanAmpTp1(:, 3:end), meanAmpTp2(:, 3:end), ...
        varianceTp1(:, 3:end), varianceTp2(:, 3:end), ...
        maxAbsAmpTp1(:, 3:end), maxAbsAmpTp2(:, 3:end), ...
        peakLatTp1(:, 3:end), peakLatTp2(:, 3:end), ...
        mpTp1(:, 3:end), mpTp2(:, 3:end), ...
        posAreaTp1(:, 3:end), posAreaTp2(:, 3:end), ...
        negAreaTp1(:, 3:end), negAreaTp2(:, 3:end)];
end
function lat = getPeakLatency(tbl, sourceTbl)
    [~, i] = max(tbl);
    lat = sourceTbl.Time(i);
end
function mp = getMpRatio(tbl, sourceTbl)
    [amp, i] = max(tbl);
    lat = sourceTbl.Time(i);
    mp = amp / lat;
end
function featureTbl = extractFreqDomainFeature(dataTbl, tp1, tp2, chArray, samplingRate, limit)
    suffix1 = '_tp1';   suffix2 = '_tp2';
    if(limit == 40)
        prefixes = {'pdelta_', 'ptheta_', 'palpha_', 'pbeta_', 'pgamma_', 'ptotal_'};
        freqRanges = [1 4; 4 8; 8 13; 13 30; 30 40; 1 40];
    elseif(limit == 30)
        prefixes = {'pdelta_', 'ptheta_', 'palpha_', 'pbeta_', 'ptotal_'};
        freqRanges = [1 4; 4 8; 8 13; 13 30; 1 30];
    end
    
    featureTbl = table();
    for i = 1:height(freqRanges)
        ftTp1 = varfun(@(x) bandpower(x, samplingRate, freqRanges(i, :)), dataTbl(tp1, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
        ftTp1 = concatColName(prefixes{i}, ftTp1, suffix1);
        ftTp2 = varfun(@(x) bandpower(x, samplingRate, freqRanges(i, :)), dataTbl(tp2, :), 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
        ftTp2 = concatColName(prefixes{i}, ftTp2, suffix2);
        
        featureTbl = [featureTbl, ftTp1(:, 3:end), ftTp2(:, 3:end)];
    end
end
function tbl = concatColName(prefix, tbl, suffix)
    tbl.Properties.VariableNames = strcat(repmat({prefix}, 1, width(tbl)), tbl.Properties.VariableNames, repmat({suffix}, 1, width(tbl)));
end