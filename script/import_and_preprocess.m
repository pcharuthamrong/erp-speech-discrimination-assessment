function import_and_preprocess(csv, setname, subject, session, experiment, group)
    [event_data, event_match_data] = read_openvibe_csv(csv);

    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    EEG = pop_importdata('dataformat','array','nbchan',0,'data','eeg_data','setname',setname,'srate',256,'subject',subject,'pnts',0,'condition',experiment,'xmin',0,'group',group,'chanlocs','..\\script\\channel_location.ced');
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'gui','off'); 
    EEG = eeg_checkset( EEG );
    EEG = pop_importevent( EEG, 'event',event_data,'fields',{'type','latency','duration'},'timeunit',1);
    [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
    EEG = eeg_checkset( EEG );
    if (strcmp(experiment, '1a') || strcmp(experiment, '1b') || strcmp(experiment, '1c') || strcmp(experiment, '2'))
        EEG = pop_importevent( EEG, 'event',event_match_data,'fields',{'type','latency','duration'},'timeunit',1,'align',0);
        [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
        EEG = eeg_checkset( EEG );
    end
    EEG = pop_saveset( EEG, 'filename',strcat(setname,'.set'),'filepath','..\\data\\eeglab\\raw\\');
    [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
    
    % filter 1 - 40 Hz
    EEG = pop_eegfiltnew(EEG, 'locutoff',1,'plotfreqz',0);
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'overwrite','on','gui','off'); 
    EEG = pop_eegfiltnew(EEG, 'hicutoff',40,'plotfreqz',0);
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'overwrite','on','gui','off');
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename',strcat(setname,'_filtered.set'),'filepath','..\\data\\eeglab\\filtered\\');
    
    % clean & re-reference
    EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion',5,'ChannelCriterion',0.8,'LineNoiseCriterion',4,'Highpass','off','BurstCriterion',30,'WindowCriterion','off','BurstRejection','on','Distance','Euclidian');
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'overwrite','on','gui','off'); 
    EEG = pop_reref( EEG, [],'interpchan',[]);
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'overwrite','on','gui','off');
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename',strcat(setname,'_clean.set'),'filepath','..\\data\\eeglab\\clean\\');
    
    % extract epoch
    if(strcmp(experiment, '1a') || strcmp(experiment, '1b') || strcmp(experiment, '1c') || strcmp(experiment, '2'))
        EEG = pop_epoch( EEG, {  '26'  '27'  }, [-0.1           0.9], 'newname', strcat(setname,'_epoch'), 'epochinfo', 'yes');
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'overwrite','on','gui','off');
    elseif(strcmp(experiment, '3'))
        EEG = pop_epoch( EEG, {  '1'  '2'  }, [-0.1           0.4], 'newname', strcat(setname,'_epoch'), 'epochinfo', 'yes');
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'overwrite','on','gui','off');
    end
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename',strcat(setname,'_epoch.set'),'filepath','..\\data\\eeglab\\epoch\\');
    
    select_epoch(setname, experiment);
    
    eeglab redraw;
end
