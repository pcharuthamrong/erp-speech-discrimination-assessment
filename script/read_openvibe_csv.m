function [event_data, event_match_data] = read_openvibe_csv(filename)
    dir_path = '..\\data\\openvibe\\';
    file_ext = '.csv';
    file_path = strcat(dir_path, filename, file_ext);
    data = readmatrix(file_path);

    %{
    separate different data (column number based on .csv file for 8 channel)
    1. time (col 1)
    2. eeg signal (col 3 to 10)
        2.1 transpose
            openvibe: rows are data points, columns are channels
            eeglab: rows are channels, columns are data points
        2.2 adjust unit
            openvibe: volt
            eeglab: microvolt
    3. event data (col 11 to 13)
        3.1 only select rows with events
    %}
    time_data = data(:, 1);
    eeg_data_original = data(:, 3:10);
    eeg_data_transposed = transpose(eeg_data_original);
    eeg_data = eeg_data_transposed.*1e+03;
    assignin('base', 'eeg_data', eeg_data);
    
    event_data_all = data(:, 11:13);
    event_data = event_data_all(any(~isnan(event_data_all), 2),:);

    % check if match / mismatch condition for exp 1a, 1c, 2
    % check which picture is correct for exp 1b
    event_match_data = [];
    last_audio_event = '';
    eventId = eventIdentifiers;
    for i = 1:size(event_data, 1)
        current_event = event_data(i,1);
        % if current event is audio then remember which word
        if(current_event == eventId.SOUND1_LOUD || current_event == eventId.SOUND1_QUIET)
            last_audio_event = 'word1';
            continue;
        elseif(current_event == eventId.SOUND2_LOUD || current_event == eventId.SOUND2_QUIET)
            last_audio_event = 'word2';
            continue;
        end
        % if current event is image then check if match / mismatch
        if(current_event == eventId.IMAGE_CHICKEN || current_event == eventId.IMAGE_WORD1)
            if(strcmp(last_audio_event,'word1'))
                new_event = eventId.MATCH;
            elseif(strcmp(last_audio_event,'word2'))
                new_event = eventId.MISMATCH;
            end
        elseif(current_event == eventId.IMAGE_EGG || current_event == eventId.IMAGE_WORD2)
            if(strcmp(last_audio_event,'word1'))
                new_event = eventId.MISMATCH;
            elseif(strcmp(last_audio_event,'word2'))
                new_event = eventId.MATCH;
            end
        elseif(current_event == eventId.IMAGE_HORSE || current_event == eventId.IMAGE_DOG)
            new_event = eventId.MISMATCH;
        end
        event_match_data = [event_match_data; [new_event event_data(i, 2:end)]];
    end

    % import to eeglab: eeg_data, event_data, event_match_data
end