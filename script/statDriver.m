clc;

% options
alpha = 0.05;
statChoice = 'kruskalwallis'; % 'anova' 'kruskalwallis'
cType = 'bonferroni';
compVar = 37; % the variable to be compared; see table variable names
% 2: accuracy, 37: Cz Beta/Alpha
include1a = true;
include1b = true;
include1c = true;
include2 = true;
include3 = true;

% read summary table for each method
summaryStruct.table1a = load('summaryTable1a.mat').summaryTbl;
summaryStruct.table1b = load('summaryTable1b.mat').summaryTbl;
summaryStruct.table1c = load('summaryTable1c.mat').summaryTbl;
summaryStruct.table2 = load('summaryTable2.mat').summaryTbl;
summaryStruct.table3 = load('summaryTable3.mat').summaryTbl;

compVarName = summaryStruct.table1a.Properties.VariableNames{compVar};
data = [];
group = [];
if(include1a)
    tempData = summaryStruct.table1a{:,compVar};
    tempGroup = repelem({'1a'},height(tempData));
    data = [data tempData'];
    group = [group tempGroup];
end
if(include1b)
    tempData = summaryStruct.table1b{:,compVar};
    tempGroup = repelem({'1b'},height(tempData));
    data = [data tempData'];
    group = [group tempGroup];
end
if(include1c)
    tempData = summaryStruct.table1c{:,compVar};
    tempGroup = repelem({'1c'},height(tempData));
    data = [data tempData'];
    group = [group tempGroup];
end
if(include2)
    tempData = summaryStruct.table2{:,compVar};
    tempGroup = repelem({'2'},height(tempData));
    data = [data tempData'];
    group = [group tempGroup];
end
if(include3)
    tempData = summaryStruct.table3{:,compVar};
    tempGroup = repelem({'3'},height(tempData));
    data = [data tempData'];
    group = [group tempGroup];
end

if (strcmp(statChoice, 'anova'))
    [p,anovaTbl,stats] = anova1(data, group, 'off');
elseif (strcmp(statChoice, 'kruskalwallis'))
    [p,anovaTbl,stats] = kruskalwallis(data, group, 'on');
end
[c,m,h] = multcompare(stats, 'Alpha', alpha, 'CType', cType, 'Display', 'off');

fprintf('Multiple comparison of means - %s\n', compVarName);
fprintf('Test: %s\t\tCType: %s\n', statChoice, cType);
fprintf('Numbered group:');
if(include1a)
    fprintf(' 1a');
end
if(include1b)
    fprintf(' 1b');
end
if(include1c)
    fprintf(' 1c');
end
if(include2)
    fprintf(' 2');
end
if(include3)
    fprintf(' 3');
end
fprintf('\n\n');
result = array2table(c, 'VariableNames', {'A', 'B' , 'Lower CI', 'estimate', 'Upper CI', 'p-value'});
disp(result);
disp(anovaTbl);
disp(stats);