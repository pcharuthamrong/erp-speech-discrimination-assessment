function select_epoch(setname, experiment)
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    EEG = pop_loadset('filename',strcat(setname,'_epoch.set'),'filepath','..\\data\\eeglab\\epoch\\');
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );
    if(strcmp(experiment, '1a') || strcmp(experiment, '1b') || strcmp(experiment, '1c') || strcmp(experiment, '2'))
        EEG = pop_selectevent( EEG, 'type',26,'renametype','match','deleteevents','off','deleteepochs','on','invertepochs','off');
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname', strcat(setname,'_match'),'gui','off'); 
        EEG = eeg_checkset( EEG );
        EEG = pop_editset(EEG, 'condition', 'match', 'run', []);
        EEG = pop_editset(EEG, 'group', experiment, 'run', []);
        [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
        EEG = eeg_checkset( EEG );
        EEG = pop_saveset( EEG, 'filename',strcat(setname,'_match.set'),'filepath','..\\data\\eeglab\\separate_condition\\');
        EEG = pop_saveset( EEG, 'filename',strcat(setname,'_match.set'),'filepath',strcat('..\\data\\eeglab\\manual_inspect\\',experiment,'\\'));
        [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'retrieve',1,'study',0); 
        EEG = eeg_checkset( EEG );
        EEG = pop_selectevent( EEG, 'type',27,'renametype','mismatch','deleteevents','off','deleteepochs','on','invertepochs','off');
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',strcat(setname,'_mismatch'),'gui','off'); 
        EEG = eeg_checkset( EEG );
        EEG = pop_editset(EEG, 'condition', 'mismatch', 'run', []);
        EEG = pop_editset(EEG, 'group', experiment, 'run', []);
        [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
        EEG = eeg_checkset( EEG );
        EEG = pop_saveset( EEG, 'filename',strcat(setname,'_mismatch.set'),'filepath','..\\data\\eeglab\\separate_condition\\');
        EEG = pop_saveset( EEG, 'filename',strcat(setname,'_mismatch.set'),'filepath',strcat('..\\data\\eeglab\\manual_inspect\\',experiment,'\\'));
        [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
    elseif(strcmp(experiment, '3'))
        EEG = pop_selectevent( EEG, 'type',1,'renametype','standard','deleteevents','off','deleteepochs','on','invertepochs','off');
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname', strcat(setname,'_standard'),'gui','off'); 
        EEG = eeg_checkset( EEG );
        EEG = pop_editset(EEG, 'condition', 'standard', 'run', []);
        EEG = pop_editset(EEG, 'group', experiment, 'run', []);
        [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
        EEG = eeg_checkset( EEG );
        EEG = pop_saveset( EEG, 'filename',strcat(setname,'_standard.set'),'filepath','..\\data\\eeglab\\separate_condition\\');
        EEG = pop_saveset( EEG, 'filename',strcat(setname,'_standard.set'),'filepath',strcat('..\\data\\eeglab\\manual_inspect\\',experiment,'\\'));
        [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'retrieve',1,'study',0); 
        EEG = eeg_checkset( EEG );
        EEG = pop_selectevent( EEG, 'type',2,'renametype','deviant','deleteevents','off','deleteepochs','on','invertepochs','off');
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname',strcat(setname,'_deviant'),'gui','off'); 
        EEG = eeg_checkset( EEG );
        EEG = pop_editset(EEG, 'condition', 'deviant', 'run', []);
        EEG = pop_editset(EEG, 'group', experiment, 'run', []);
        [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
        EEG = eeg_checkset( EEG );
        EEG = pop_saveset( EEG, 'filename',strcat(setname,'_deviant.set'),'filepath','..\\data\\eeglab\\separate_condition\\');
        EEG = pop_saveset( EEG, 'filename',strcat(setname,'_deviant.set'),'filepath',strcat('..\\data\\eeglab\\manual_inspect\\',experiment,'\\'));
        [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
    end
    eeglab redraw;
end