clc;
filenameFile = '..\\data\\summary\\preprocessed_filename.xlsx';
path = '..\\data\\preprocessed\\';
accuracyFile = '..\\data\\summary\\calculate avg acc.xlsx';

% options
method = '3';
% choiceModel = 'CSPRawFeatures_LinearSVM_1_40Hz';
choiceModel = 'CSPRawFeatures_LinearSVM_1_30Hz';

chArray = [2 3 6]; % Fz Cz Pz
samplingRate = 256;
framePerEpochMap = containers.Map({'1a', '1b', '1c', '2', '3'}, {256, 256, 256, 256, 128});
framePerEpoch = framePerEpochMap(method);
participantCountMap = containers.Map({'1a', '1b', '1c', '2', '3'}, {9, 10, 7, 25, 26});
participantCount = participantCountMap(method);
channelCount = 8;
betaFreq = [13 30];
alphaFreq = [8 13];

filenameTable = readFilename(filenameFile, method);
if (strcmp(method, '3'))
    tp1s = 100; tp1e = 250;
    tp2s = 250; tp2e = 400;
else
    tp1s = 200; tp1e = 400;
    tp2s = 500; tp2e = 800;
end

summaryTbl = initStatSummaryTbl(participantCount);
for i = 1:2:height(filenameTable)
% for i = 1:2:1
    for j = 0:1
        filename = filenameTable.filename{i+j}(1:end-4);
        folder = filenameTable.folder{i+j};
        setname = filename(1:strfind(filename, '_') - 1);
        participant = filename(2:strfind(filename, 's') - 1);
        % 1:match/standard, 2:mismatch/deviant
        conditionName = filename(strfind(filename, '_') + 1:end);
        if (strcmp(conditionName, 'match') || strcmp(conditionName, 'standard'))
            eegTable1 = readEegFile(path, folder, filename, framePerEpoch);
            epochCount1 = height(eegTable1) / framePerEpoch;
            eegTable1 = eegTable1(eegTable1.Time > 0, :);
        elseif (strcmp(conditionName, 'mismatch') || strcmp(conditionName, 'deviant'))
            eegTable2 = readEegFile(path, folder, filename, framePerEpoch);
            epochCount2 = height(eegTable2) / framePerEpoch;
            eegTable2 = eegTable2(eegTable2.Time > 0, :);
        end
    end
    % update frame per epoch to match the cut epochs
    framePerEpochCut = height(eegTable1(eegTable1.Epoch == 1,:)); % equal frames over all epoch, condition
    % compute average waveform
    eegAvg1 = averageOverEpoch(eegTable1, epochCount1, framePerEpochCut, chArray);
    eegAvg2 = averageOverEpoch(eegTable2, epochCount2, framePerEpochCut, chArray);
    % compute difference wave
    eegDiff = eegAvg2 - eegAvg1; % mismatch - match / deviant - standard
    areaDiff = trapz(eegDiff);
    % convert back to table & add time col
    avgTable1 = [eegTable1(eegTable1.Epoch == 1,1), array2table(eegAvg1)];
    avgTable1.Properties.VariableNames = eegTable1(:,[1,chArray]).Properties.VariableNames;
    avgTable2 = [eegTable2(eegTable1.Epoch == 1,1), array2table(eegAvg2)];
    avgTable2.Properties.VariableNames = eegTable2(:,[1,chArray]).Properties.VariableNames;
    % find peak amplitude & latency for both time periods for both conditions
    [peakAmp1tp1, peakLat1tp1] = findPeak(avgTable1, tp1s, tp1e, chArray);
    [peakAmp1tp2, peakLat1tp2] = findPeak(avgTable1, tp2s, tp2e, chArray);
    [peakAmp2tp1, peakLat2tp1] = findPeak(avgTable2, tp1s, tp1e, chArray);
    [peakAmp2tp2, peakLat2tp2] = findPeak(avgTable2, tp2s, tp2e, chArray);
    % placeholder for accuracy
    accuracy = -1;
    % compute attention-related measures
    tempEegTable2 = eegTable2;
    tempEegTable2.Epoch = tempEegTable2.Epoch + 40;
    combinedEegTable = [eegTable1; tempEegTable2];
    [beta, alpha, baRatio] = computeAttentionRelated(combinedEegTable, samplingRate, betaFreq, alphaFreq, chArray);
    % store in summary table
    summaryTbl((i-1)/2+1,:) = array2table( ...
        [str2double(participant), accuracy, areaDiff, ...
        peakAmp1tp1', peakLat1tp1', peakAmp1tp2', peakLat1tp2', ...
        peakAmp2tp1', peakLat2tp1', peakAmp2tp2', peakLat2tp2', ...
        beta, alpha, baRatio]);
end
% read accuracy from file & add to summaryTbl
accArray = readAccuracy(method, accuracyFile, choiceModel);
summaryTbl.Accuracy = accArray;
save(strcat('summaryTable', method ,'.mat'), 'summaryTbl');

function filenameTable = readFilename(filenameFile, folderSelect)
    % read filenames from excel sheet
    opts = detectImportOptions(filenameFile);
    opts.DataRange = 'A2';
    opts = setvartype(opts, {'folder', 'filename'}, 'char');
    wholeFilenameTable = readtable(filenameFile, opts);
    filenameTable = wholeFilenameTable(strcmp(wholeFilenameTable.folder, folderSelect), :);
end
function dataTbl = readEegFile(path, folder, filename, framePerEpoch)
    % read file
    file = strcat(path, folder, '\\', filename, '.csv');
    opts = detectImportOptions(file);
    dataTbl = readtable(file, opts);
    % add epoch label to each row
    epoch_count = height(dataTbl) / framePerEpoch;
    temp = [];
    for i = 1:epoch_count
        temp = [temp;repmat(i, framePerEpoch, 1)];
    end
    dataTbl.Epoch = temp;
end
function eegAvg = averageOverEpoch(eegTbl, epochCount, framePerEpochCut, chArray)
    eegTemp = zeros(framePerEpochCut, width(chArray));
    for epoch = 1:epochCount
       eegTemp = eegTemp + table2array(eegTbl(eegTbl.Epoch == epoch, chArray));
    end
    eegAvg = eegTemp / epochCount;
end
function [peakAmp, peakLat] = findPeak(avgTbl, timeStart, timeEnd, chArray)
    avgTbl = avgTbl(avgTbl.Time >= timeStart & avgTbl.Time <= timeEnd,:);
    absEeg = table2array(avgTbl(:, 2:end));
    [~, peakIdx] = max(absEeg);
    peakAmp = zeros(width(chArray), 1);
    for i=1:width(chArray)
        peakAmp(i) = avgTbl{peakIdx(i), i+1};
    end
    peakLat = avgTbl.Time(peakIdx);
end
function accArray = readAccuracy(method, accuracyFile, choiceModel)
    % turn warning for modified varnames off
    warning('off','MATLAB:table:ModifiedAndSavedVarnames');
    % read accuracy from file
    numVarsMap = containers.Map({'1a', '1b', '1c', '2', '3'}, {11, 10, 10, 10, 10});
    dataRangeMap = containers.Map({'1a', '1b', '1c', '2', '3'}, ...
        {'B2:L10', 'B2:K11', 'B2:K8', 'B2:K26', 'B2:K27'});
    opts = detectImportOptions(accuracyFile, ...
        'Sheet', method, 'NumVariables', numVarsMap(method));
    opts.VariableNamesRange = 'B1';
    opts.DataRange = dataRangeMap(method);
    accTbl = readtable(accuracyFile, opts);
    % turn warning for modified varnames back on
    warning('on','MATLAB:table:ModifiedAndSavedVarnames');
    
    accArray = accTbl.(choiceModel);
end
function [beta, alpha, baRatio] = computeAttentionRelated(eegTbl, samplingRate, betaFreq, alphaFreq, chArray)
    % compute beta, alpha power + beta/alpha
    betaTbl = varfun(@(x) bandpower(x, samplingRate, betaFreq), eegTbl, 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    alphaTbl = varfun(@(x) bandpower(x, samplingRate, alphaFreq), eegTbl, 'InputVariables', chArray, 'GroupingVariable', 'Epoch');
    beta = table2array(varfun(@mean, betaTbl, 'InputVariables', 3:2+width(chArray)));
    alpha = table2array(varfun(@mean, alphaTbl, 'InputVariables', 3:2+width(chArray)));
    baRatio = beta./alpha;
end