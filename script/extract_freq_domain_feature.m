function predictor_table = extract_freq_domain_feature(eeg_table, tp1, tp2, ch_array, sampling_rate, limit)
    suffix1 = '_tp1';   suffix2 = '_tp2';
    if(limit == 40)
        prefixes = {'pdelta_', 'ptheta_', 'palpha_', 'pbeta_', 'pgamma_', 'ptotal_'};
        freq_ranges = [1 4; 4 8; 8 13; 13 30; 30 40; 1 40];
    elseif(limit == 30)
        prefixes = {'pdelta_', 'ptheta_', 'palpha_', 'pbeta_', 'ptotal_'};
        freq_ranges = [1 4; 4 8; 8 13; 13 30; 1 30];
    end
    
    predictor_table = table();
    for i = 1:height(freq_ranges)
        ft_tp1 = varfun(@(x) bandpower(x, sampling_rate, freq_ranges(i, :)), eeg_table(tp1, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
        ft_tp1 = concat_col_name(prefixes{i}, ft_tp1, suffix1);
        ft_tp2 = varfun(@(x) bandpower(x, sampling_rate, freq_ranges(i, :)), eeg_table(tp2, :), 'InputVariables', ch_array, 'GroupingVariable', 'Epoch');
        ft_tp2 = concat_col_name(prefixes{i}, ft_tp2, suffix2);
        
        predictor_table = [predictor_table, ft_tp1(:, 3:end), ft_tp2(:, 3:end)];
    end
end