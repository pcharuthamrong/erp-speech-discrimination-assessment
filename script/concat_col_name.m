function tbl = concat_col_name(prefix, tbl, suffix)
    tbl.Properties.VariableNames = strcat(repmat({prefix}, 1, width(tbl)), tbl.Properties.VariableNames, repmat({suffix}, 1, width(tbl)));
end