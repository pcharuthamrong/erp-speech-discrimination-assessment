function predictor_table = extract_raw_feature(eeg_table, epoch_count, channel_count)
    col_per_ch = size(eeg_table{eeg_table.Epoch == 1,2}, 1);
    col_count = col_per_ch * channel_count;
    predictor_table = table('Size', [epoch_count col_count], 'VariableTypes', repmat(["double"], 1, col_count));
    for e = 1:epoch_count
        for c = 1:channel_count
            start_col = col_per_ch * (c - 1) + 1;
            end_col = col_per_ch * c;
            predictor_table(e, start_col:end_col) = num2cell(eeg_table{eeg_table.Epoch == e,c+1}');
        end
    end
end