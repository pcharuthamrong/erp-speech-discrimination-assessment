% ----------------------------------------------------------------------- %
%                           H    Y    D    R    A                         %
% ----------------------------------------------------------------------- %
% Function 'csp' trains a Common Spatial Pattern (CSP) filter bank.       %
%                                                                         %
%   Input parameters:                                                     %
%       - X1:   Signal for the positive class, dimensions [C x T], where  %
%               C is the no. channels and T the no. samples.              %
%       - X2:   Signal for the negative class, dimensions [C x T], where  %
%               C is the no. channels and T the no. samples.              %
%                                                                         %
%   Output variables:                                                     %
%       - W:        Filter matrix (mixing matrix, forward model). Note that
%                   the columns of W are the spatial filters.             %
%       - lambda:   Eigenvalues of each filter.                           %
%       - A:        Demixing matrix (backward model).                     %
% ----------------------------------------------------------------------- %
%   Versions:                                                             %
%       - 1.0:     (19/07/2019) Original script.                          %
% ----------------------------------------------------------------------- %
%   Script information:                                                   %
%       - Version:      1.0.                                              %
%       - Author:       V. Martínez-Cagigal                               %
%       - Date:         19/07/2019                                        %
% ----------------------------------------------------------------------- %
%   Example of use:                                                       %
%       csp_example;                                                      %
% ----------------------------------------------------------------------- %
%   References:                                                           %
%       [1]     Blankertz, B., Tomioka, R., Lemm, S., Kawanabe, M., &     %
%               Muller, K. R. (2007). Optimizing spatial filters for robust 
%               EEG single-trial analysis. IEEE Signal processing magazine, 
%               25(1), 41-56.                                             %
% ----------------------------------------------------------------------- %
function [W, lambda, A] = csp(X1, X2)

    % Error detection
    if nargin < 2, error('Not enough parameters.'); end
    if length(size(X1))~=2 || length(size(X2))~=2
        error('The size of trial signals must be [C x T]');
    end
    
    % Compute the covariance matrix of each class
    S1 = cov(X1');   % S1~[C x C]
    S2 = cov(X2');   % S2~[C x C]

    % Solve the eigenvalue problem S1·W = l·S2·W
    [W,L] = eig(S1, S1 + S2);   % Mixing matrix W (spatial filters are columns)
    lambda = diag(L);           % Eigenvalues
    A = (inv(W))';              % Demixing matrix
    
    % Further notes:
    %   - CSP filtered signal is computed as: X_csp = W'*X;
end

% Copyright (c) 2019, Víctor Martínez-Cagigal
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of University of Valladolid nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
