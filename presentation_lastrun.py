﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2020.1.2),
    on June 25, 2021, at 20:44
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

from __future__ import absolute_import, division

from psychopy import locale_setup
from psychopy import prefs
prefs.hardware['audioLib'] = 'pyo'
from psychopy import sound, gui, visual, core, data, event, logging, clock
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard



# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '2020.1.2'
expName = 'presentation'  # from the Builder filename that created this script
expInfo = {'participant': '', 'session': '', 'experiment_type': ''}
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/psychopy/%s_%s_%s_%s_%s' % (expInfo['participant'], expName, expInfo['experiment_type'], expInfo['session'], expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='F:\\Workspace\\erp-speech-discrimination-assessment\\presentation_lastrun.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run before the window creation

# Setup the Window
win = visual.Window(
    size=[1280, 720], fullscr=True, screen=0, 
    winType='pyglet', allowGUI=False, allowStencil=False,
    monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
    blendMode='avg', useFBO=True, 
    units='height')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard()

# Initialize components for Routine "init"
initClock = core.Clock()
#| padding (8 bytes) | event id (8 bytes) | timestamp (8 bytes) |

# import
import socket
from time import time

# host and port
HOST = '127.0.0.1'
PORT = 15361

# event identifiers
SOUND1_LOUD = 0x00000001
SOUND2_LOUD = 0x00000002
SOUND1_QUIET = 0x00000003
SOUND2_QUIET = 0x00000004

IMAGE_CHICKEN = 0x00000011
IMAGE_EGG = 0x00000012
IMAGE_HORSE = 0x00000013
IMAGE_DOG = 0x00000014
IMAGE_WORD1 = 0x00000015
IMAGE_WORD2 = 0x00000016

# artificial delay (ms)
DELAY = 0

# transform value into array of byte values (little-endian order)
def to_byte(val, length):
    for i in range(length):
        yield val % 256
        val //= 256

'''# send trigger based on event type
def send_trigger(component, trigger_sent, component_type, event_type):
    if component_type == 'sound':
        get_event = get_sound_event()
    elif component_type == 'image':
        get_event = get_image_event(
    
    if component.status == STARTED and not trigger_sent:
        send_tag(get_event(event_type))
        return True
    False'''

# create and send tag with padding, event_id, and timestamp
def send_tag(event):
    padding = [0]*8
    event_id = list(to_byte(event, 8))
    
    # timestamp either posix time in ms or 0 (acquisition will timestamp)
    timestamp = list(to_byte(int(time() * 1000) + DELAY, 8))
    #timestamp = list(to_byte(0, 8))
    
    # send tag
    s.sendall(bytearray(padding+event_id+timestamp))
    #print(event, event_id)

# check event type
def get_sound_event():
    if sound_type == 1 and volume == 1:
        event = SOUND1_LOUD
    elif sound_type == 1 and volume == 0.5:
        event = SOUND1_QUIET
    elif sound_type == 2 and volume == 1:
        event = SOUND2_LOUD
    elif sound_type == 2 and volume == 0.5:
        event = SOUND2_QUIET
    return event

def get_image_event(image_type):
    if image_type == 1:
        event = IMAGE_CHICKEN
    elif image_type == 2:
        event = IMAGE_EGG
    elif image_type == 3:
        event = IMAGE_HORSE
    elif image_type == 4:
        event = IMAGE_DOG
    elif image_type == 5:
        event = IMAGE_WORD1
    elif image_type == 6:
        event = IMAGE_WORD2
    return event


# connect
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
# selected rows
SOLUTION_ROWS = '0, 6'
SINGLE_CHOICE_ROWS = '0:8'
MULTIPLE_CHOICE_ROWS = ''
SPELLING_ROWS = '8:16'
AUDIO_ROWS = ''

# loop reps
SOLUTION_REPS = 5
SINGLE_CHOICE_REPS = 10
MULTIPLE_CHOICE_REPS = 3
AUDIO_REPS = 30

experiment_type = expInfo['experiment_type']

if experiment_type == '1a':
    show_solution = False
    run_single_visual_trial = True
    run_multiple_visual_trial = False
    run_audio_trial = False
    trial_loop_rows = SINGLE_CHOICE_ROWS
elif experiment_type == '1b':
    show_solution = False
    run_single_visual_trial = False
    run_multiple_visual_trial = True
    run_audio_trial = False
    trial_loop_rows = MULTIPLE_CHOICE_ROWS
elif experiment_type == '1c':
    show_solution = True
    run_single_visual_trial = True
    run_multiple_visual_trial = False
    run_audio_trial = False
    trial_loop_rows = SINGLE_CHOICE_ROWS
elif experiment_type == '2':
    show_solution = False
    run_single_visual_trial = True
    run_multiple_visual_trial = False
    run_audio_trial = False
    trial_loop_rows = SPELLING_ROWS
elif experiment_type == '3':
    show_solution = False
    run_single_visual_trial = False
    run_multiple_visual_trial = False
    run_audio_trial = True
    trial_loop_rows = AUDIO_ROWS
else:
    print('incorrect experiment type')
    endExpNow = True

# assign reps based on whether the loop is run or not
if show_solution:
    solution_trial_reps = SOLUTION_REPS
else:
    solution_trial_reps = 0

if run_single_visual_trial:
    single_visual_trial_reps = SINGLE_CHOICE_REPS
else:
    single_visual_trial_reps = 0

if run_multiple_visual_trial:
    multiple_visual_trial_reps = MULTIPLE_CHOICE_REPS
else:
    multiple_visual_trial_reps = 0

if run_audio_trial:
    audio_trial_reps = AUDIO_REPS
else:
    audio_trial_reps = 0


#print(trial_loop_condition)

# Initialize components for Routine "solution_instr"
solution_instrClock = core.Clock()
solution_instr_image = visual.ImageStim(
    win=win,
    name='solution_instr_image', 
    image='./res/solution_instr.png', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-1.0)
solution_instr_key_resp = keyboard.Keyboard()

# Initialize components for Routine "show_solution"
show_solutionClock = core.Clock()
solution_sound = sound.Sound('A', secs=0.500, stereo=True, hamming=True,
    name='solution_sound')
solution_sound.setVolume(1.0)
solution_image = visual.ImageStim(
    win=win,
    name='solution_image', 
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-2.0)

# Initialize components for Routine "single_visual_instr"
single_visual_instrClock = core.Clock()
single_visual_instr_image = visual.ImageStim(
    win=win,
    name='single_visual_instr_image', 
    image='./res/single_visual_instr.png', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-1.0)
single_visual_instr_key_resp = keyboard.Keyboard()

# Initialize components for Routine "single_visual_trial"
single_visual_trialClock = core.Clock()
single_visual_trial_sound = sound.Sound('A', secs=0.5, stereo=True, hamming=True,
    name='single_visual_trial_sound')
single_visual_trial_sound.setVolume(1.0)
single_visual_trial_image = visual.ImageStim(
    win=win,
    name='single_visual_trial_image', 
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-2.0)
single_visual_answer_prompt = visual.ImageStim(
    win=win,
    name='single_visual_answer_prompt', 
    image='res/answer_prompt.png', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-3.0)
single_visual_trial_key_resp = keyboard.Keyboard()

# Initialize components for Routine "multiple_visual_instr"
multiple_visual_instrClock = core.Clock()
multiple_visual_instr_image = visual.ImageStim(
    win=win,
    name='multiple_visual_instr_image', 
    image='./res/multiple_visual_instr.png', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-1.0)
multiple_visual_instr_key_resp = keyboard.Keyboard()

# Initialize components for Routine "multiple_visual_trial"
multiple_visual_trialClock = core.Clock()
multiple_visual_trial_sound = sound.Sound('A', secs=0.5, stereo=True, hamming=True,
    name='multiple_visual_trial_sound')
multiple_visual_trial_sound.setVolume(1.0)
multiple_visual_trial_image_1 = visual.ImageStim(
    win=win,
    name='multiple_visual_trial_image_1', 
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-2.0)
multiple_visual_trial_text_1 = visual.TextStim(win=win, name='multiple_visual_trial_text_1',
    text='1',
    font='TH Sarabun New',
    pos=(0.5, 0.4), height=0.18, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-3.0);
multiple_visual_trial_image_2 = visual.ImageStim(
    win=win,
    name='multiple_visual_trial_image_2', 
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-4.0)
multiple_visual_trial_text_2 = visual.TextStim(win=win, name='multiple_visual_trial_text_2',
    text='2',
    font='TH Sarabun New',
    pos=(0.5, 0.4), height=0.18, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-5.0);
multiple_visual_trial_image_3 = visual.ImageStim(
    win=win,
    name='multiple_visual_trial_image_3', 
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-6.0)
multiple_visual_trial_text_3 = visual.TextStim(win=win, name='multiple_visual_trial_text_3',
    text='3',
    font='TH Sarabun New',
    pos=(0.5, 0.4), height=0.18, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-7.0);
multiple_visual_trial_image_4 = visual.ImageStim(
    win=win,
    name='multiple_visual_trial_image_4', 
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-8.0)
multiple_visual_trial_text_4 = visual.TextStim(win=win, name='multiple_visual_trial_text_4',
    text='4',
    font='TH Sarabun New',
    pos=(0.5, 0.4), height=0.18, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-9.0);
multiple_visual_trial_answer_prompt = visual.ImageStim(
    win=win,
    name='multiple_visual_trial_answer_prompt', 
    image='res/multi_answer_prompt.png', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-10.0)
multiple_visual_trial_key_resp = keyboard.Keyboard()

# Initialize components for Routine "audio_instr"
audio_instrClock = core.Clock()
audio_instr_image = visual.ImageStim(
    win=win,
    name='audio_instr_image', 
    image='./res/audio_instr.png', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-1.0)
audio_instr_key_resp = keyboard.Keyboard()

# Initialize components for Routine "audio_trial"
audio_trialClock = core.Clock()
focusing_cross = visual.TextStim(win=win, name='focusing_cross',
    text='+',
    font='TH Sarabun New',
    pos=(0, 0.1), height=0.4, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
audio_trial_sound = sound.Sound('A', secs=0.5, stereo=True, hamming=True,
    name='audio_trial_sound')
audio_trial_sound.setVolume(1.0)

# Initialize components for Routine "feedback"
feedbackClock = core.Clock()
template_image = visual.ImageStim(
    win=win,
    name='template_image', 
    image='./res/feedbackTemplate.png', mask=None,
    ori=0, pos=(0, 0), size=None,
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-1.0)
msg_correct_text = visual.TextStim(win=win, name='msg_correct_text',
    text='default text',
    font='TH Sarabun New',
    pos=(0, 0.21), height=0.18, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
msg_total_text = visual.TextStim(win=win, name='msg_total_text',
    text='default text',
    font='TH Sarabun New',
    pos=(0, 0.025), height=0.18, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-3.0);
msg_mean_resp_time_text = visual.TextStim(win=win, name='msg_mean_resp_time_text',
    text='default text',
    font='TH Sarabun New',
    pos=(0.25, -0.155), height=0.18, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-4.0);
feedback_key_resp = keyboard.Keyboard()

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "init"-------
continueRoutine = True
# update component parameters for each repeat
# keep track of which components have finished
initComponents = []
for thisComponent in initComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
initClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "init"-------
while continueRoutine:
    # get current time
    t = initClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=initClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in initComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "init"-------
for thisComponent in initComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "init" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "solution_instr"-------
continueRoutine = True
# update component parameters for each repeat
if not show_solution:
    continueRoutine = False

solution_instr_key_resp.keys = []
solution_instr_key_resp.rt = []
_solution_instr_key_resp_allKeys = []
# keep track of which components have finished
solution_instrComponents = [solution_instr_image, solution_instr_key_resp]
for thisComponent in solution_instrComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
solution_instrClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "solution_instr"-------
while continueRoutine:
    # get current time
    t = solution_instrClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=solution_instrClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *solution_instr_image* updates
    if solution_instr_image.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        solution_instr_image.frameNStart = frameN  # exact frame index
        solution_instr_image.tStart = t  # local t and not account for scr refresh
        solution_instr_image.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(solution_instr_image, 'tStartRefresh')  # time at next scr refresh
        solution_instr_image.setAutoDraw(True)
    
    # *solution_instr_key_resp* updates
    waitOnFlip = False
    if solution_instr_key_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        solution_instr_key_resp.frameNStart = frameN  # exact frame index
        solution_instr_key_resp.tStart = t  # local t and not account for scr refresh
        solution_instr_key_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(solution_instr_key_resp, 'tStartRefresh')  # time at next scr refresh
        solution_instr_key_resp.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(solution_instr_key_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(solution_instr_key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if solution_instr_key_resp.status == STARTED and not waitOnFlip:
        theseKeys = solution_instr_key_resp.getKeys(keyList=None, waitRelease=False)
        _solution_instr_key_resp_allKeys.extend(theseKeys)
        if len(_solution_instr_key_resp_allKeys):
            solution_instr_key_resp.keys = _solution_instr_key_resp_allKeys[-1].name  # just the last key pressed
            solution_instr_key_resp.rt = _solution_instr_key_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in solution_instrComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "solution_instr"-------
for thisComponent in solution_instrComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('solution_instr_image.started', solution_instr_image.tStartRefresh)
thisExp.addData('solution_instr_image.stopped', solution_instr_image.tStopRefresh)
# the Routine "solution_instr" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
solution_loop = data.TrialHandler(nReps=solution_trial_reps, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('single_visual_conditions.xlsx', selection=SOLUTION_ROWS),
    seed=None, name='solution_loop')
thisExp.addLoop(solution_loop)  # add the loop to the experiment
thisSolution_loop = solution_loop.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisSolution_loop.rgb)
if thisSolution_loop != None:
    for paramName in thisSolution_loop:
        exec('{} = thisSolution_loop[paramName]'.format(paramName))

for thisSolution_loop in solution_loop:
    currentLoop = solution_loop
    # abbreviate parameter names if possible (e.g. rgb = thisSolution_loop.rgb)
    if thisSolution_loop != None:
        for paramName in thisSolution_loop:
            exec('{} = thisSolution_loop[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "show_solution"-------
    continueRoutine = True
    routineTimer.add(2.000000)
    # update component parameters for each repeat
    sound_trigger_sent = False
    image_trigger_sent = False
    
    #print(sound_type, volume, sound_file)
    #print(image_type, image_file)
    solution_sound.setSound(sound_file, secs=0.500, hamming=True)
    solution_sound.setVolume(volume, log=False)
    solution_image.setImage(image_file)
    # keep track of which components have finished
    show_solutionComponents = [solution_sound, solution_image]
    for thisComponent in show_solutionComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    show_solutionClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "show_solution"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = show_solutionClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=show_solutionClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        if solution_sound.status == STARTED and not sound_trigger_sent:
            send_tag(get_sound_event())
            sound_trigger_sent = True
        
        if solution_image.status == STARTED and not image_trigger_sent:
            send_tag(get_image_event(image_type))
            image_trigger_sent = True
        
        # start/stop solution_sound
        if solution_sound.status == NOT_STARTED and tThisFlip >= 0.500-frameTolerance:
            # keep track of start time/frame for later
            solution_sound.frameNStart = frameN  # exact frame index
            solution_sound.tStart = t  # local t and not account for scr refresh
            solution_sound.tStartRefresh = tThisFlipGlobal  # on global time
            solution_sound.play(when=win)  # sync with win flip
        if solution_sound.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > solution_sound.tStartRefresh + 0.500-frameTolerance:
                # keep track of stop time/frame for later
                solution_sound.tStop = t  # not accounting for scr refresh
                solution_sound.frameNStop = frameN  # exact frame index
                win.timeOnFlip(solution_sound, 'tStopRefresh')  # time at next scr refresh
                solution_sound.stop()
        
        # *solution_image* updates
        if solution_image.status == NOT_STARTED and tThisFlip >= 1.000-frameTolerance:
            # keep track of start time/frame for later
            solution_image.frameNStart = frameN  # exact frame index
            solution_image.tStart = t  # local t and not account for scr refresh
            solution_image.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(solution_image, 'tStartRefresh')  # time at next scr refresh
            solution_image.setAutoDraw(True)
        if solution_image.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > solution_image.tStartRefresh + 1.000-frameTolerance:
                # keep track of stop time/frame for later
                solution_image.tStop = t  # not accounting for scr refresh
                solution_image.frameNStop = frameN  # exact frame index
                win.timeOnFlip(solution_image, 'tStopRefresh')  # time at next scr refresh
                solution_image.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in show_solutionComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "show_solution"-------
    for thisComponent in show_solutionComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    solution_sound.stop()  # ensure sound has stopped at end of routine
    solution_loop.addData('solution_sound.started', solution_sound.tStartRefresh)
    solution_loop.addData('solution_sound.stopped', solution_sound.tStopRefresh)
    solution_loop.addData('solution_image.started', solution_image.tStartRefresh)
    solution_loop.addData('solution_image.stopped', solution_image.tStopRefresh)
    thisExp.nextEntry()
    
# completed solution_trial_reps repeats of 'solution_loop'


# ------Prepare to start Routine "single_visual_instr"-------
continueRoutine = True
# update component parameters for each repeat
if not run_single_visual_trial:
    continueRoutine = False
single_visual_instr_key_resp.keys = []
single_visual_instr_key_resp.rt = []
_single_visual_instr_key_resp_allKeys = []
# keep track of which components have finished
single_visual_instrComponents = [single_visual_instr_image, single_visual_instr_key_resp]
for thisComponent in single_visual_instrComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
single_visual_instrClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "single_visual_instr"-------
while continueRoutine:
    # get current time
    t = single_visual_instrClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=single_visual_instrClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *single_visual_instr_image* updates
    if single_visual_instr_image.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        single_visual_instr_image.frameNStart = frameN  # exact frame index
        single_visual_instr_image.tStart = t  # local t and not account for scr refresh
        single_visual_instr_image.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(single_visual_instr_image, 'tStartRefresh')  # time at next scr refresh
        single_visual_instr_image.setAutoDraw(True)
    
    # *single_visual_instr_key_resp* updates
    waitOnFlip = False
    if single_visual_instr_key_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        single_visual_instr_key_resp.frameNStart = frameN  # exact frame index
        single_visual_instr_key_resp.tStart = t  # local t and not account for scr refresh
        single_visual_instr_key_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(single_visual_instr_key_resp, 'tStartRefresh')  # time at next scr refresh
        single_visual_instr_key_resp.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(single_visual_instr_key_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(single_visual_instr_key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if single_visual_instr_key_resp.status == STARTED and not waitOnFlip:
        theseKeys = single_visual_instr_key_resp.getKeys(keyList=None, waitRelease=False)
        _single_visual_instr_key_resp_allKeys.extend(theseKeys)
        if len(_single_visual_instr_key_resp_allKeys):
            single_visual_instr_key_resp.keys = _single_visual_instr_key_resp_allKeys[-1].name  # just the last key pressed
            single_visual_instr_key_resp.rt = _single_visual_instr_key_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in single_visual_instrComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "single_visual_instr"-------
for thisComponent in single_visual_instrComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('single_visual_instr_image.started', single_visual_instr_image.tStartRefresh)
thisExp.addData('single_visual_instr_image.stopped', single_visual_instr_image.tStopRefresh)
# the Routine "single_visual_instr" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
single_visual_trial_loop = data.TrialHandler(nReps=single_visual_trial_reps, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('single_visual_conditions.xlsx', selection=trial_loop_rows),
    seed=None, name='single_visual_trial_loop')
thisExp.addLoop(single_visual_trial_loop)  # add the loop to the experiment
thisSingle_visual_trial_loop = single_visual_trial_loop.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisSingle_visual_trial_loop.rgb)
if thisSingle_visual_trial_loop != None:
    for paramName in thisSingle_visual_trial_loop:
        exec('{} = thisSingle_visual_trial_loop[paramName]'.format(paramName))

for thisSingle_visual_trial_loop in single_visual_trial_loop:
    currentLoop = single_visual_trial_loop
    # abbreviate parameter names if possible (e.g. rgb = thisSingle_visual_trial_loop.rgb)
    if thisSingle_visual_trial_loop != None:
        for paramName in thisSingle_visual_trial_loop:
            exec('{} = thisSingle_visual_trial_loop[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "single_visual_trial"-------
    continueRoutine = True
    # update component parameters for each repeat
    sound_trigger_sent = False
    image_trigger_sent = False
    
    #print(sound_type, volume, sound_file)
    #print(image_type, image_file)
    single_visual_trial_sound.setSound(sound_file, secs=0.5, hamming=True)
    single_visual_trial_sound.setVolume(volume, log=False)
    single_visual_trial_image.setImage(image_file)
    single_visual_trial_key_resp.keys = []
    single_visual_trial_key_resp.rt = []
    _single_visual_trial_key_resp_allKeys = []
    # keep track of which components have finished
    single_visual_trialComponents = [single_visual_trial_sound, single_visual_trial_image, single_visual_answer_prompt, single_visual_trial_key_resp]
    for thisComponent in single_visual_trialComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    single_visual_trialClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "single_visual_trial"-------
    while continueRoutine:
        # get current time
        t = single_visual_trialClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=single_visual_trialClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        if single_visual_trial_sound.status == STARTED and not sound_trigger_sent:
            send_tag(get_sound_event())
            sound_trigger_sent = True
        
        if single_visual_trial_image.status == STARTED and not image_trigger_sent:
            send_tag(get_image_event(image_type))
            image_trigger_sent = True
        
        # start/stop single_visual_trial_sound
        if single_visual_trial_sound.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
            # keep track of start time/frame for later
            single_visual_trial_sound.frameNStart = frameN  # exact frame index
            single_visual_trial_sound.tStart = t  # local t and not account for scr refresh
            single_visual_trial_sound.tStartRefresh = tThisFlipGlobal  # on global time
            single_visual_trial_sound.play(when=win)  # sync with win flip
        if single_visual_trial_sound.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > single_visual_trial_sound.tStartRefresh + 0.5-frameTolerance:
                # keep track of stop time/frame for later
                single_visual_trial_sound.tStop = t  # not accounting for scr refresh
                single_visual_trial_sound.frameNStop = frameN  # exact frame index
                win.timeOnFlip(single_visual_trial_sound, 'tStopRefresh')  # time at next scr refresh
                single_visual_trial_sound.stop()
        
        # *single_visual_trial_image* updates
        if single_visual_trial_image.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            single_visual_trial_image.frameNStart = frameN  # exact frame index
            single_visual_trial_image.tStart = t  # local t and not account for scr refresh
            single_visual_trial_image.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(single_visual_trial_image, 'tStartRefresh')  # time at next scr refresh
            single_visual_trial_image.setAutoDraw(True)
        if single_visual_trial_image.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > single_visual_trial_image.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                single_visual_trial_image.tStop = t  # not accounting for scr refresh
                single_visual_trial_image.frameNStop = frameN  # exact frame index
                win.timeOnFlip(single_visual_trial_image, 'tStopRefresh')  # time at next scr refresh
                single_visual_trial_image.setAutoDraw(False)
        
        # *single_visual_answer_prompt* updates
        if single_visual_answer_prompt.status == NOT_STARTED and tThisFlip >= 2-frameTolerance:
            # keep track of start time/frame for later
            single_visual_answer_prompt.frameNStart = frameN  # exact frame index
            single_visual_answer_prompt.tStart = t  # local t and not account for scr refresh
            single_visual_answer_prompt.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(single_visual_answer_prompt, 'tStartRefresh')  # time at next scr refresh
            single_visual_answer_prompt.setAutoDraw(True)
        
        # *single_visual_trial_key_resp* updates
        waitOnFlip = False
        if single_visual_trial_key_resp.status == NOT_STARTED and tThisFlip >= 2-frameTolerance:
            # keep track of start time/frame for later
            single_visual_trial_key_resp.frameNStart = frameN  # exact frame index
            single_visual_trial_key_resp.tStart = t  # local t and not account for scr refresh
            single_visual_trial_key_resp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(single_visual_trial_key_resp, 'tStartRefresh')  # time at next scr refresh
            single_visual_trial_key_resp.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(single_visual_trial_key_resp.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(single_visual_trial_key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if single_visual_trial_key_resp.status == STARTED and not waitOnFlip:
            theseKeys = single_visual_trial_key_resp.getKeys(keyList=['1', '2'], waitRelease=False)
            _single_visual_trial_key_resp_allKeys.extend(theseKeys)
            if len(_single_visual_trial_key_resp_allKeys):
                single_visual_trial_key_resp.keys = _single_visual_trial_key_resp_allKeys[-1].name  # just the last key pressed
                single_visual_trial_key_resp.rt = _single_visual_trial_key_resp_allKeys[-1].rt
                # was this correct?
                if (single_visual_trial_key_resp.keys == str(correct_ans)) or (single_visual_trial_key_resp.keys == correct_ans):
                    single_visual_trial_key_resp.corr = 1
                else:
                    single_visual_trial_key_resp.corr = 0
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in single_visual_trialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "single_visual_trial"-------
    for thisComponent in single_visual_trialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    single_visual_trial_sound.stop()  # ensure sound has stopped at end of routine
    single_visual_trial_loop.addData('single_visual_trial_sound.started', single_visual_trial_sound.tStartRefresh)
    single_visual_trial_loop.addData('single_visual_trial_sound.stopped', single_visual_trial_sound.tStopRefresh)
    single_visual_trial_loop.addData('single_visual_trial_image.started', single_visual_trial_image.tStartRefresh)
    single_visual_trial_loop.addData('single_visual_trial_image.stopped', single_visual_trial_image.tStopRefresh)
    single_visual_trial_loop.addData('single_visual_answer_prompt.started', single_visual_answer_prompt.tStartRefresh)
    single_visual_trial_loop.addData('single_visual_answer_prompt.stopped', single_visual_answer_prompt.tStopRefresh)
    # check responses
    if single_visual_trial_key_resp.keys in ['', [], None]:  # No response was made
        single_visual_trial_key_resp.keys = None
        # was no response the correct answer?!
        if str(correct_ans).lower() == 'none':
           single_visual_trial_key_resp.corr = 1;  # correct non-response
        else:
           single_visual_trial_key_resp.corr = 0;  # failed to respond (incorrectly)
    # store data for single_visual_trial_loop (TrialHandler)
    single_visual_trial_loop.addData('single_visual_trial_key_resp.keys',single_visual_trial_key_resp.keys)
    single_visual_trial_loop.addData('single_visual_trial_key_resp.corr', single_visual_trial_key_resp.corr)
    if single_visual_trial_key_resp.keys != None:  # we had a response
        single_visual_trial_loop.addData('single_visual_trial_key_resp.rt', single_visual_trial_key_resp.rt)
    single_visual_trial_loop.addData('single_visual_trial_key_resp.started', single_visual_trial_key_resp.tStartRefresh)
    single_visual_trial_loop.addData('single_visual_trial_key_resp.stopped', single_visual_trial_key_resp.tStopRefresh)
    # the Routine "single_visual_trial" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed single_visual_trial_reps repeats of 'single_visual_trial_loop'


# ------Prepare to start Routine "multiple_visual_instr"-------
continueRoutine = True
# update component parameters for each repeat
if not run_multiple_visual_trial:
    continueRoutine = False
multiple_visual_instr_key_resp.keys = []
multiple_visual_instr_key_resp.rt = []
_multiple_visual_instr_key_resp_allKeys = []
# keep track of which components have finished
multiple_visual_instrComponents = [multiple_visual_instr_image, multiple_visual_instr_key_resp]
for thisComponent in multiple_visual_instrComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
multiple_visual_instrClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "multiple_visual_instr"-------
while continueRoutine:
    # get current time
    t = multiple_visual_instrClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=multiple_visual_instrClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *multiple_visual_instr_image* updates
    if multiple_visual_instr_image.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        multiple_visual_instr_image.frameNStart = frameN  # exact frame index
        multiple_visual_instr_image.tStart = t  # local t and not account for scr refresh
        multiple_visual_instr_image.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(multiple_visual_instr_image, 'tStartRefresh')  # time at next scr refresh
        multiple_visual_instr_image.setAutoDraw(True)
    
    # *multiple_visual_instr_key_resp* updates
    waitOnFlip = False
    if multiple_visual_instr_key_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        multiple_visual_instr_key_resp.frameNStart = frameN  # exact frame index
        multiple_visual_instr_key_resp.tStart = t  # local t and not account for scr refresh
        multiple_visual_instr_key_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(multiple_visual_instr_key_resp, 'tStartRefresh')  # time at next scr refresh
        multiple_visual_instr_key_resp.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(multiple_visual_instr_key_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(multiple_visual_instr_key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if multiple_visual_instr_key_resp.status == STARTED and not waitOnFlip:
        theseKeys = multiple_visual_instr_key_resp.getKeys(keyList=None, waitRelease=False)
        _multiple_visual_instr_key_resp_allKeys.extend(theseKeys)
        if len(_multiple_visual_instr_key_resp_allKeys):
            multiple_visual_instr_key_resp.keys = _multiple_visual_instr_key_resp_allKeys[-1].name  # just the last key pressed
            multiple_visual_instr_key_resp.rt = _multiple_visual_instr_key_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in multiple_visual_instrComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "multiple_visual_instr"-------
for thisComponent in multiple_visual_instrComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('multiple_visual_instr_image.started', multiple_visual_instr_image.tStartRefresh)
thisExp.addData('multiple_visual_instr_image.stopped', multiple_visual_instr_image.tStopRefresh)
# the Routine "multiple_visual_instr" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
multiple_visual_trial_loop = data.TrialHandler(nReps=multiple_visual_trial_reps, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('multiple_visual_conditions.xlsx', selection=trial_loop_rows),
    seed=None, name='multiple_visual_trial_loop')
thisExp.addLoop(multiple_visual_trial_loop)  # add the loop to the experiment
thisMultiple_visual_trial_loop = multiple_visual_trial_loop.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisMultiple_visual_trial_loop.rgb)
if thisMultiple_visual_trial_loop != None:
    for paramName in thisMultiple_visual_trial_loop:
        exec('{} = thisMultiple_visual_trial_loop[paramName]'.format(paramName))

for thisMultiple_visual_trial_loop in multiple_visual_trial_loop:
    currentLoop = multiple_visual_trial_loop
    # abbreviate parameter names if possible (e.g. rgb = thisMultiple_visual_trial_loop.rgb)
    if thisMultiple_visual_trial_loop != None:
        for paramName in thisMultiple_visual_trial_loop:
            exec('{} = thisMultiple_visual_trial_loop[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "multiple_visual_trial"-------
    continueRoutine = True
    # update component parameters for each repeat
    sound_trigger_sent = False
    image_1_trigger_sent = False
    image_2_trigger_sent = False
    image_3_trigger_sent = False
    image_4_trigger_sent = False
    
    multiple_visual_trial_sound.setSound(sound_file, secs=0.5, hamming=True)
    multiple_visual_trial_sound.setVolume(volume, log=False)
    multiple_visual_trial_image_1.setImage(image_1_file)
    multiple_visual_trial_image_2.setImage(image_2_file)
    multiple_visual_trial_image_3.setImage(image_3_file)
    multiple_visual_trial_image_4.setImage(image_4_file)
    multiple_visual_trial_key_resp.keys = []
    multiple_visual_trial_key_resp.rt = []
    _multiple_visual_trial_key_resp_allKeys = []
    # keep track of which components have finished
    multiple_visual_trialComponents = [multiple_visual_trial_sound, multiple_visual_trial_image_1, multiple_visual_trial_text_1, multiple_visual_trial_image_2, multiple_visual_trial_text_2, multiple_visual_trial_image_3, multiple_visual_trial_text_3, multiple_visual_trial_image_4, multiple_visual_trial_text_4, multiple_visual_trial_answer_prompt, multiple_visual_trial_key_resp]
    for thisComponent in multiple_visual_trialComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    multiple_visual_trialClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "multiple_visual_trial"-------
    while continueRoutine:
        # get current time
        t = multiple_visual_trialClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=multiple_visual_trialClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        if multiple_visual_trial_sound.status == STARTED and not sound_trigger_sent:
            send_tag(get_sound_event())
            sound_trigger_sent = True
        
        if multiple_visual_trial_image_1.status == STARTED and not image_1_trigger_sent:
            send_tag(get_image_event(image_1_type))
            image_1_trigger_sent = True
        
        if multiple_visual_trial_image_2.status == STARTED and not image_2_trigger_sent:
            send_tag(get_image_event(image_2_type))
            image_2_trigger_sent = True
        
        if multiple_visual_trial_image_3.status == STARTED and not image_3_trigger_sent:
            send_tag(get_image_event(image_3_type))
            image_3_trigger_sent = True
        
        if multiple_visual_trial_image_4.status == STARTED and not image_4_trigger_sent:
            send_tag(get_image_event(image_4_type))
            image_4_trigger_sent = True
        
        
        # start/stop multiple_visual_trial_sound
        if multiple_visual_trial_sound.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_sound.frameNStart = frameN  # exact frame index
            multiple_visual_trial_sound.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_sound.tStartRefresh = tThisFlipGlobal  # on global time
            multiple_visual_trial_sound.play(when=win)  # sync with win flip
        if multiple_visual_trial_sound.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > multiple_visual_trial_sound.tStartRefresh + 0.5-frameTolerance:
                # keep track of stop time/frame for later
                multiple_visual_trial_sound.tStop = t  # not accounting for scr refresh
                multiple_visual_trial_sound.frameNStop = frameN  # exact frame index
                win.timeOnFlip(multiple_visual_trial_sound, 'tStopRefresh')  # time at next scr refresh
                multiple_visual_trial_sound.stop()
        
        # *multiple_visual_trial_image_1* updates
        if multiple_visual_trial_image_1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_image_1.frameNStart = frameN  # exact frame index
            multiple_visual_trial_image_1.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_image_1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(multiple_visual_trial_image_1, 'tStartRefresh')  # time at next scr refresh
            multiple_visual_trial_image_1.setAutoDraw(True)
        if multiple_visual_trial_image_1.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > multiple_visual_trial_image_1.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                multiple_visual_trial_image_1.tStop = t  # not accounting for scr refresh
                multiple_visual_trial_image_1.frameNStop = frameN  # exact frame index
                win.timeOnFlip(multiple_visual_trial_image_1, 'tStopRefresh')  # time at next scr refresh
                multiple_visual_trial_image_1.setAutoDraw(False)
        
        # *multiple_visual_trial_text_1* updates
        if multiple_visual_trial_text_1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_text_1.frameNStart = frameN  # exact frame index
            multiple_visual_trial_text_1.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_text_1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(multiple_visual_trial_text_1, 'tStartRefresh')  # time at next scr refresh
            multiple_visual_trial_text_1.setAutoDraw(True)
        if multiple_visual_trial_text_1.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > multiple_visual_trial_text_1.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                multiple_visual_trial_text_1.tStop = t  # not accounting for scr refresh
                multiple_visual_trial_text_1.frameNStop = frameN  # exact frame index
                win.timeOnFlip(multiple_visual_trial_text_1, 'tStopRefresh')  # time at next scr refresh
                multiple_visual_trial_text_1.setAutoDraw(False)
        
        # *multiple_visual_trial_image_2* updates
        if multiple_visual_trial_image_2.status == NOT_STARTED and tThisFlip >= 2-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_image_2.frameNStart = frameN  # exact frame index
            multiple_visual_trial_image_2.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_image_2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(multiple_visual_trial_image_2, 'tStartRefresh')  # time at next scr refresh
            multiple_visual_trial_image_2.setAutoDraw(True)
        if multiple_visual_trial_image_2.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > multiple_visual_trial_image_2.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                multiple_visual_trial_image_2.tStop = t  # not accounting for scr refresh
                multiple_visual_trial_image_2.frameNStop = frameN  # exact frame index
                win.timeOnFlip(multiple_visual_trial_image_2, 'tStopRefresh')  # time at next scr refresh
                multiple_visual_trial_image_2.setAutoDraw(False)
        
        # *multiple_visual_trial_text_2* updates
        if multiple_visual_trial_text_2.status == NOT_STARTED and tThisFlip >= 2-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_text_2.frameNStart = frameN  # exact frame index
            multiple_visual_trial_text_2.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_text_2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(multiple_visual_trial_text_2, 'tStartRefresh')  # time at next scr refresh
            multiple_visual_trial_text_2.setAutoDraw(True)
        if multiple_visual_trial_text_2.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > multiple_visual_trial_text_2.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                multiple_visual_trial_text_2.tStop = t  # not accounting for scr refresh
                multiple_visual_trial_text_2.frameNStop = frameN  # exact frame index
                win.timeOnFlip(multiple_visual_trial_text_2, 'tStopRefresh')  # time at next scr refresh
                multiple_visual_trial_text_2.setAutoDraw(False)
        
        # *multiple_visual_trial_image_3* updates
        if multiple_visual_trial_image_3.status == NOT_STARTED and tThisFlip >= 3-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_image_3.frameNStart = frameN  # exact frame index
            multiple_visual_trial_image_3.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_image_3.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(multiple_visual_trial_image_3, 'tStartRefresh')  # time at next scr refresh
            multiple_visual_trial_image_3.setAutoDraw(True)
        if multiple_visual_trial_image_3.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > multiple_visual_trial_image_3.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                multiple_visual_trial_image_3.tStop = t  # not accounting for scr refresh
                multiple_visual_trial_image_3.frameNStop = frameN  # exact frame index
                win.timeOnFlip(multiple_visual_trial_image_3, 'tStopRefresh')  # time at next scr refresh
                multiple_visual_trial_image_3.setAutoDraw(False)
        
        # *multiple_visual_trial_text_3* updates
        if multiple_visual_trial_text_3.status == NOT_STARTED and tThisFlip >= 3-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_text_3.frameNStart = frameN  # exact frame index
            multiple_visual_trial_text_3.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_text_3.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(multiple_visual_trial_text_3, 'tStartRefresh')  # time at next scr refresh
            multiple_visual_trial_text_3.setAutoDraw(True)
        if multiple_visual_trial_text_3.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > multiple_visual_trial_text_3.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                multiple_visual_trial_text_3.tStop = t  # not accounting for scr refresh
                multiple_visual_trial_text_3.frameNStop = frameN  # exact frame index
                win.timeOnFlip(multiple_visual_trial_text_3, 'tStopRefresh')  # time at next scr refresh
                multiple_visual_trial_text_3.setAutoDraw(False)
        
        # *multiple_visual_trial_image_4* updates
        if multiple_visual_trial_image_4.status == NOT_STARTED and tThisFlip >= 4-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_image_4.frameNStart = frameN  # exact frame index
            multiple_visual_trial_image_4.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_image_4.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(multiple_visual_trial_image_4, 'tStartRefresh')  # time at next scr refresh
            multiple_visual_trial_image_4.setAutoDraw(True)
        if multiple_visual_trial_image_4.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > multiple_visual_trial_image_4.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                multiple_visual_trial_image_4.tStop = t  # not accounting for scr refresh
                multiple_visual_trial_image_4.frameNStop = frameN  # exact frame index
                win.timeOnFlip(multiple_visual_trial_image_4, 'tStopRefresh')  # time at next scr refresh
                multiple_visual_trial_image_4.setAutoDraw(False)
        
        # *multiple_visual_trial_text_4* updates
        if multiple_visual_trial_text_4.status == NOT_STARTED and tThisFlip >= 4-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_text_4.frameNStart = frameN  # exact frame index
            multiple_visual_trial_text_4.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_text_4.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(multiple_visual_trial_text_4, 'tStartRefresh')  # time at next scr refresh
            multiple_visual_trial_text_4.setAutoDraw(True)
        if multiple_visual_trial_text_4.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > multiple_visual_trial_text_4.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                multiple_visual_trial_text_4.tStop = t  # not accounting for scr refresh
                multiple_visual_trial_text_4.frameNStop = frameN  # exact frame index
                win.timeOnFlip(multiple_visual_trial_text_4, 'tStopRefresh')  # time at next scr refresh
                multiple_visual_trial_text_4.setAutoDraw(False)
        
        # *multiple_visual_trial_answer_prompt* updates
        if multiple_visual_trial_answer_prompt.status == NOT_STARTED and tThisFlip >= 5-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_answer_prompt.frameNStart = frameN  # exact frame index
            multiple_visual_trial_answer_prompt.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_answer_prompt.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(multiple_visual_trial_answer_prompt, 'tStartRefresh')  # time at next scr refresh
            multiple_visual_trial_answer_prompt.setAutoDraw(True)
        
        # *multiple_visual_trial_key_resp* updates
        waitOnFlip = False
        if multiple_visual_trial_key_resp.status == NOT_STARTED and tThisFlip >= 5-frameTolerance:
            # keep track of start time/frame for later
            multiple_visual_trial_key_resp.frameNStart = frameN  # exact frame index
            multiple_visual_trial_key_resp.tStart = t  # local t and not account for scr refresh
            multiple_visual_trial_key_resp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(multiple_visual_trial_key_resp, 'tStartRefresh')  # time at next scr refresh
            multiple_visual_trial_key_resp.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(multiple_visual_trial_key_resp.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(multiple_visual_trial_key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if multiple_visual_trial_key_resp.status == STARTED and not waitOnFlip:
            theseKeys = multiple_visual_trial_key_resp.getKeys(keyList=['1', '2', '3', '4'], waitRelease=False)
            _multiple_visual_trial_key_resp_allKeys.extend(theseKeys)
            if len(_multiple_visual_trial_key_resp_allKeys):
                multiple_visual_trial_key_resp.keys = _multiple_visual_trial_key_resp_allKeys[-1].name  # just the last key pressed
                multiple_visual_trial_key_resp.rt = _multiple_visual_trial_key_resp_allKeys[-1].rt
                # was this correct?
                if (multiple_visual_trial_key_resp.keys == str(correct_ans)) or (multiple_visual_trial_key_resp.keys == correct_ans):
                    multiple_visual_trial_key_resp.corr = 1
                else:
                    multiple_visual_trial_key_resp.corr = 0
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in multiple_visual_trialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "multiple_visual_trial"-------
    for thisComponent in multiple_visual_trialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    multiple_visual_trial_sound.stop()  # ensure sound has stopped at end of routine
    multiple_visual_trial_loop.addData('multiple_visual_trial_sound.started', multiple_visual_trial_sound.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_sound.stopped', multiple_visual_trial_sound.tStopRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_image_1.started', multiple_visual_trial_image_1.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_image_1.stopped', multiple_visual_trial_image_1.tStopRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_text_1.started', multiple_visual_trial_text_1.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_text_1.stopped', multiple_visual_trial_text_1.tStopRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_image_2.started', multiple_visual_trial_image_2.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_image_2.stopped', multiple_visual_trial_image_2.tStopRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_text_2.started', multiple_visual_trial_text_2.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_text_2.stopped', multiple_visual_trial_text_2.tStopRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_image_3.started', multiple_visual_trial_image_3.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_image_3.stopped', multiple_visual_trial_image_3.tStopRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_text_3.started', multiple_visual_trial_text_3.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_text_3.stopped', multiple_visual_trial_text_3.tStopRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_image_4.started', multiple_visual_trial_image_4.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_image_4.stopped', multiple_visual_trial_image_4.tStopRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_text_4.started', multiple_visual_trial_text_4.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_text_4.stopped', multiple_visual_trial_text_4.tStopRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_answer_prompt.started', multiple_visual_trial_answer_prompt.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_answer_prompt.stopped', multiple_visual_trial_answer_prompt.tStopRefresh)
    # check responses
    if multiple_visual_trial_key_resp.keys in ['', [], None]:  # No response was made
        multiple_visual_trial_key_resp.keys = None
        # was no response the correct answer?!
        if str(correct_ans).lower() == 'none':
           multiple_visual_trial_key_resp.corr = 1;  # correct non-response
        else:
           multiple_visual_trial_key_resp.corr = 0;  # failed to respond (incorrectly)
    # store data for multiple_visual_trial_loop (TrialHandler)
    multiple_visual_trial_loop.addData('multiple_visual_trial_key_resp.keys',multiple_visual_trial_key_resp.keys)
    multiple_visual_trial_loop.addData('multiple_visual_trial_key_resp.corr', multiple_visual_trial_key_resp.corr)
    if multiple_visual_trial_key_resp.keys != None:  # we had a response
        multiple_visual_trial_loop.addData('multiple_visual_trial_key_resp.rt', multiple_visual_trial_key_resp.rt)
    multiple_visual_trial_loop.addData('multiple_visual_trial_key_resp.started', multiple_visual_trial_key_resp.tStartRefresh)
    multiple_visual_trial_loop.addData('multiple_visual_trial_key_resp.stopped', multiple_visual_trial_key_resp.tStopRefresh)
    # the Routine "multiple_visual_trial" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed multiple_visual_trial_reps repeats of 'multiple_visual_trial_loop'


# ------Prepare to start Routine "audio_instr"-------
continueRoutine = True
# update component parameters for each repeat
if not run_audio_trial:
    continueRoutine = False
audio_instr_key_resp.keys = []
audio_instr_key_resp.rt = []
_audio_instr_key_resp_allKeys = []
# keep track of which components have finished
audio_instrComponents = [audio_instr_image, audio_instr_key_resp]
for thisComponent in audio_instrComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
audio_instrClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "audio_instr"-------
while continueRoutine:
    # get current time
    t = audio_instrClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=audio_instrClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *audio_instr_image* updates
    if audio_instr_image.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        audio_instr_image.frameNStart = frameN  # exact frame index
        audio_instr_image.tStart = t  # local t and not account for scr refresh
        audio_instr_image.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(audio_instr_image, 'tStartRefresh')  # time at next scr refresh
        audio_instr_image.setAutoDraw(True)
    
    # *audio_instr_key_resp* updates
    waitOnFlip = False
    if audio_instr_key_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        audio_instr_key_resp.frameNStart = frameN  # exact frame index
        audio_instr_key_resp.tStart = t  # local t and not account for scr refresh
        audio_instr_key_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(audio_instr_key_resp, 'tStartRefresh')  # time at next scr refresh
        audio_instr_key_resp.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(audio_instr_key_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(audio_instr_key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if audio_instr_key_resp.status == STARTED and not waitOnFlip:
        theseKeys = audio_instr_key_resp.getKeys(keyList=None, waitRelease=False)
        _audio_instr_key_resp_allKeys.extend(theseKeys)
        if len(_audio_instr_key_resp_allKeys):
            audio_instr_key_resp.keys = _audio_instr_key_resp_allKeys[-1].name  # just the last key pressed
            audio_instr_key_resp.rt = _audio_instr_key_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in audio_instrComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "audio_instr"-------
for thisComponent in audio_instrComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('audio_instr_image.started', audio_instr_image.tStartRefresh)
thisExp.addData('audio_instr_image.stopped', audio_instr_image.tStopRefresh)
# the Routine "audio_instr" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
audio_trial_loop = data.TrialHandler(nReps=audio_trial_reps, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('audio_conditions.xlsx', selection=trial_loop_rows),
    seed=None, name='audio_trial_loop')
thisExp.addLoop(audio_trial_loop)  # add the loop to the experiment
thisAudio_trial_loop = audio_trial_loop.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisAudio_trial_loop.rgb)
if thisAudio_trial_loop != None:
    for paramName in thisAudio_trial_loop:
        exec('{} = thisAudio_trial_loop[paramName]'.format(paramName))

for thisAudio_trial_loop in audio_trial_loop:
    currentLoop = audio_trial_loop
    # abbreviate parameter names if possible (e.g. rgb = thisAudio_trial_loop.rgb)
    if thisAudio_trial_loop != None:
        for paramName in thisAudio_trial_loop:
            exec('{} = thisAudio_trial_loop[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "audio_trial"-------
    continueRoutine = True
    routineTimer.add(1.500000)
    # update component parameters for each repeat
    sound_trigger_sent = False
    
    audio_trial_sound.setSound(sound_file, secs=0.5, hamming=True)
    audio_trial_sound.setVolume(volume, log=False)
    # keep track of which components have finished
    audio_trialComponents = [focusing_cross, audio_trial_sound]
    for thisComponent in audio_trialComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    audio_trialClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "audio_trial"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = audio_trialClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=audio_trialClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        if audio_trial_sound.status == STARTED and not sound_trigger_sent:
            send_tag(get_sound_event())
            sound_trigger_sent = True
        
        # *focusing_cross* updates
        if focusing_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            focusing_cross.frameNStart = frameN  # exact frame index
            focusing_cross.tStart = t  # local t and not account for scr refresh
            focusing_cross.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(focusing_cross, 'tStartRefresh')  # time at next scr refresh
            focusing_cross.setAutoDraw(True)
        if focusing_cross.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > focusing_cross.tStartRefresh + 1.5-frameTolerance:
                # keep track of stop time/frame for later
                focusing_cross.tStop = t  # not accounting for scr refresh
                focusing_cross.frameNStop = frameN  # exact frame index
                win.timeOnFlip(focusing_cross, 'tStopRefresh')  # time at next scr refresh
                focusing_cross.setAutoDraw(False)
        # start/stop audio_trial_sound
        if audio_trial_sound.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
            # keep track of start time/frame for later
            audio_trial_sound.frameNStart = frameN  # exact frame index
            audio_trial_sound.tStart = t  # local t and not account for scr refresh
            audio_trial_sound.tStartRefresh = tThisFlipGlobal  # on global time
            audio_trial_sound.play(when=win)  # sync with win flip
        if audio_trial_sound.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > audio_trial_sound.tStartRefresh + 0.5-frameTolerance:
                # keep track of stop time/frame for later
                audio_trial_sound.tStop = t  # not accounting for scr refresh
                audio_trial_sound.frameNStop = frameN  # exact frame index
                win.timeOnFlip(audio_trial_sound, 'tStopRefresh')  # time at next scr refresh
                audio_trial_sound.stop()
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in audio_trialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "audio_trial"-------
    for thisComponent in audio_trialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    audio_trial_loop.addData('focusing_cross.started', focusing_cross.tStartRefresh)
    audio_trial_loop.addData('focusing_cross.stopped', focusing_cross.tStopRefresh)
    audio_trial_sound.stop()  # ensure sound has stopped at end of routine
    audio_trial_loop.addData('audio_trial_sound.started', audio_trial_sound.tStartRefresh)
    audio_trial_loop.addData('audio_trial_sound.stopped', audio_trial_sound.tStopRefresh)
    thisExp.nextEntry()
    
# completed audio_trial_reps repeats of 'audio_trial_loop'


# ------Prepare to start Routine "feedback"-------
continueRoutine = True
# update component parameters for each repeat
if experiment_type == '1a' or experiment_type == '1c' or experiment_type == '2':
    count_correct = single_visual_trial_loop.data['single_visual_trial_key_resp.corr'].sum()
    count_total = single_visual_trial_loop.data['single_visual_trial_key_resp.corr'].count()
    mean_resp_time = single_visual_trial_loop.data['single_visual_trial_key_resp.rt'].mean()
elif experiment_type == '1b':
    count_correct = multiple_visual_trial_loop.data['multiple_visual_trial_key_resp.corr'].sum()
    count_total = multiple_visual_trial_loop.data['multiple_visual_trial_key_resp.corr'].count()
    mean_resp_time = multiple_visual_trial_loop.data['multiple_visual_trial_key_resp.rt'].mean()
elif experiment_type == '3':
    count_correct = audio_trial_loop.data['audio_trial_key_resp.corr'].sum()
    count_total = audio_trial_loop.data['audio_trial_key_resp.corr'].count()
    mean_resp_time = audio_trial_loop.data['audio_trial_key_resp.rt'].mean()
else:
    continueRoutine = False

#msg = "You got %i trials correct out of %i (rt=%.2f)" %(count_correct,count_total,mean_resp_time)

msg_correct = '%i' %(count_correct)
msg_total = '%i' %(count_total)
msg_mean_resp_time = '%.2f' %(mean_resp_time)
msg_correct_text.setText(msg_correct)
msg_total_text.setText(msg_total)
msg_mean_resp_time_text.setText(msg_mean_resp_time)
feedback_key_resp.keys = []
feedback_key_resp.rt = []
_feedback_key_resp_allKeys = []
# keep track of which components have finished
feedbackComponents = [template_image, msg_correct_text, msg_total_text, msg_mean_resp_time_text, feedback_key_resp]
for thisComponent in feedbackComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
feedbackClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "feedback"-------
while continueRoutine:
    # get current time
    t = feedbackClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=feedbackClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *template_image* updates
    if template_image.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        template_image.frameNStart = frameN  # exact frame index
        template_image.tStart = t  # local t and not account for scr refresh
        template_image.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(template_image, 'tStartRefresh')  # time at next scr refresh
        template_image.setAutoDraw(True)
    
    # *msg_correct_text* updates
    if msg_correct_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        msg_correct_text.frameNStart = frameN  # exact frame index
        msg_correct_text.tStart = t  # local t and not account for scr refresh
        msg_correct_text.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(msg_correct_text, 'tStartRefresh')  # time at next scr refresh
        msg_correct_text.setAutoDraw(True)
    
    # *msg_total_text* updates
    if msg_total_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        msg_total_text.frameNStart = frameN  # exact frame index
        msg_total_text.tStart = t  # local t and not account for scr refresh
        msg_total_text.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(msg_total_text, 'tStartRefresh')  # time at next scr refresh
        msg_total_text.setAutoDraw(True)
    
    # *msg_mean_resp_time_text* updates
    if msg_mean_resp_time_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        msg_mean_resp_time_text.frameNStart = frameN  # exact frame index
        msg_mean_resp_time_text.tStart = t  # local t and not account for scr refresh
        msg_mean_resp_time_text.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(msg_mean_resp_time_text, 'tStartRefresh')  # time at next scr refresh
        msg_mean_resp_time_text.setAutoDraw(True)
    
    # *feedback_key_resp* updates
    waitOnFlip = False
    if feedback_key_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        feedback_key_resp.frameNStart = frameN  # exact frame index
        feedback_key_resp.tStart = t  # local t and not account for scr refresh
        feedback_key_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(feedback_key_resp, 'tStartRefresh')  # time at next scr refresh
        feedback_key_resp.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(feedback_key_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(feedback_key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if feedback_key_resp.status == STARTED and not waitOnFlip:
        theseKeys = feedback_key_resp.getKeys(keyList=None, waitRelease=False)
        _feedback_key_resp_allKeys.extend(theseKeys)
        if len(_feedback_key_resp_allKeys):
            feedback_key_resp.keys = _feedback_key_resp_allKeys[-1].name  # just the last key pressed
            feedback_key_resp.rt = _feedback_key_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in feedbackComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "feedback"-------
for thisComponent in feedbackComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('template_image.started', template_image.tStartRefresh)
thisExp.addData('template_image.stopped', template_image.tStopRefresh)
thisExp.addData('msg_correct_text.started', msg_correct_text.tStartRefresh)
thisExp.addData('msg_correct_text.stopped', msg_correct_text.tStopRefresh)
thisExp.addData('msg_total_text.started', msg_total_text.tStartRefresh)
thisExp.addData('msg_total_text.stopped', msg_total_text.tStopRefresh)
thisExp.addData('msg_mean_resp_time_text.started', msg_mean_resp_time_text.tStartRefresh)
thisExp.addData('msg_mean_resp_time_text.stopped', msg_mean_resp_time_text.tStopRefresh)
# the Routine "feedback" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()
s.close()

# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
